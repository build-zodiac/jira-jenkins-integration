/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * Helper for the Release Report features
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
@Named
public class ReleaseReportHelper {

	private final ConfigurationService configurationService;
	private final StateService stateService;
	private final BuildService buildService;

	@Inject
	public ReleaseReportHelper(ConfigurationService configurationService, StateService stateService, BuildService buildService) {
		this.configurationService = configurationService;
		this.stateService = stateService;
		this.buildService = buildService;
	}

	public List<Job> getBuildInformation(String issueKey) {
		if (stateService.areFeaturesEnabledForIssue(issueKey)) {
			return buildService.getByIssueKey(issueKey).stream().collect(groupingBy(Build::getJob, toList()))
			                   .entrySet().stream()
			                   .map(match -> {
				                   Job job = Job.copy(match.getKey());
				                   if (configurationService.useJobLatestBuildResultInIndexes()) {
					                   buildService.get(job, job.getLastBuild())
					                               .ifPresent(job::addBuild);
				                   } else {
					                   job.setBuilds(match.getValue());
				                   }
				                   return job;
			                   })
			                   .collect(toList());
		} else {
			return new ArrayList<>();
		}
	}

}
