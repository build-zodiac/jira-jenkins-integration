/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport;

import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.model.Result;

import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryRequest;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.query.order.SortOrder;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static org.marvelution.jji.jql.JqlHelper.createWorstBuildTerminalClause;

/**
 * Release Report Warning for Failing builds
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class FailingCiBuildsVersionWarningCategory extends AbstractIssueBasedVersionWarningCategory {

	public FailingCiBuildsVersionWarningCategory(StateService stateService, @ComponentImport SearchService searchService,
	                                             @ComponentImport I18nHelper.BeanFactory i18nHelperFactory,
	                                             @ComponentImport SoyTemplateRenderer soyTemplateRenderer,
	                                             @ComponentImport AvatarService avatarService) {
		super(stateService, searchService, i18nHelperFactory, soyTemplateRenderer, avatarService);
	}

	@Override
	protected Query getQuery(VersionWarningCategoryRequest request) {
		return JqlQueryBuilder.newBuilder()
		                      .where()
		                      .project(request.getProject().getId())
		                      .and().fixVersion(request.getVersion().getId())
		                      .and().statusCategory("done")
		                      .and().addClause(createWorstBuildTerminalClause(Result.FAILURE))
		                      .endWhere()
		                      .orderBy()
		                      .priority(SortOrder.DESC)
		                      .issueKey(SortOrder.ASC)
		                      .endOrderBy()
		                      .buildQuery();
	}

}
