/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.releasereport;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.marvelution.jji.releasereport.common.AvatarMetaData;
import org.marvelution.jji.data.services.api.StateService;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategory;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryException;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryRequest;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryResponse;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toMap;

/**
 * Issue based release report warning
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
abstract class AbstractIssueBasedVersionWarningCategory implements VersionWarningCategory {

	private static final PagerFilter FIRST_PAGE = PagerFilter.newPageAlignedFilter(0, 20);
	private static final SearchResults EMPTY_SEARCH_RESULTS = new SearchResults(emptyList(), new PagerFilter<>());
	private static final String RELEASE_REPORT_TEMPLATE_COMPLETE_MODULE_KEY =
			"com.atlassian.jira.plugins.jira-development-integration-plugin:release-report-soy-templates";
	private static final String RELEASE_WARNING_TEMPLATE_NAME = "JIRA.Templates.ReleaseReport.warningCategoryList";
	private final StateService stateService;
	private final SearchService searchService;
	private final I18nHelper.BeanFactory i18nHelperFactory;
	private final SoyTemplateRenderer soyTemplateRenderer;
	private final AvatarService avatarService;

	AbstractIssueBasedVersionWarningCategory(StateService stateService, SearchService searchService,
	                                         I18nHelper.BeanFactory i18nHelperFactory, SoyTemplateRenderer soyTemplateRenderer,
	                                         AvatarService avatarService) {
		this.stateService = stateService;
		this.searchService = searchService;
		this.i18nHelperFactory = i18nHelperFactory;
		this.soyTemplateRenderer = soyTemplateRenderer;
		this.avatarService = avatarService;
	}

	@Override
	public boolean shouldDisplay(@Nonnull VersionWarningCategoryRequest request) {
		return stateService.areFeaturesEnabledFor(request.getProject());
	}

	@Override
	public long count(@Nonnull VersionWarningCategoryRequest request) {
		try {
			return searchService.searchCount(request.getUser(), getQuery(request));
		} catch (SearchException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public VersionWarningCategoryResponse getResponse(@Nonnull VersionWarningCategoryRequest request)
			throws VersionWarningCategoryException {
		ErrorCollection errorCollection = new SimpleErrorCollection();
		Map<String, Object> model = getTemplateModel(request, errorCollection);
		return new VersionWarningCategoryResponse.Builder().setContent(renderTemplate(model))
		                                                   .setSuccessful(!errorCollection.hasAnyErrors())
		                                                   .setErrorCollection(errorCollection)
		                                                   .build();
	}

	/**
	 * Returns the {@link Query} used by this {@link VersionWarningCategory}
	 */
	protected abstract Query getQuery(VersionWarningCategoryRequest request);

	/**
	 * Returns the template model data set
	 */
	private Map<String, Object> getTemplateModel(VersionWarningCategoryRequest request, ErrorCollection errorCollection) {
		SearchResults search = searchIssues(request, i18nHelperFactory.getInstance(request.getUser()), errorCollection);
		Map<String, Object> model = new HashMap<>();
		List<Issue> issues = search.getIssues();
		model.put("issues", issues);
		model.put("unassignedAvatar", getAvatarMetaData(request.getUser(), null));
		model.put("avatars", issues.stream().collect(toMap(Issue::getKey, issue -> getAvatarMetaData(request.getUser(), issue))));
		model.put("issueCount", search.getTotal());
		model.put("jqlQuery", searchService.getJqlString(getQuery(request)));
		model.put("pluggableColumns", emptyList());
		return unmodifiableMap(model);
	}

	/**
	 * Search for issues that match the {@link #getQuery(VersionWarningCategoryRequest) query}
	 */
	private SearchResults searchIssues(VersionWarningCategoryRequest request, I18nHelper i18n, ErrorCollection errorCollection) {
		try {
			return searchService.search(request.getUser(), getQuery(request), FIRST_PAGE);
		} catch (SearchException e) {
			errorCollection.addErrorMessage(i18n.getText("release.report.warnings.fetch.issues.error"));
		}
		return EMPTY_SEARCH_RESULTS;
	}

	/**
	 * Render the SOY template provided by the jira-development-integration-plugin
	 */
	private String renderTemplate(Map<String, Object> model) throws VersionWarningCategoryException {
		try {
			StringWriter writer = new StringWriter();
			soyTemplateRenderer.render(writer, RELEASE_REPORT_TEMPLATE_COMPLETE_MODULE_KEY, RELEASE_WARNING_TEMPLATE_NAME, model);
			return writer.toString();
		} catch (SoyException e) {
			throw new VersionWarningCategoryException(e);
		}
	}

	/**
	 * Returns the {@link AvatarMetaData} object for the assignee of the specified {@link Issue issue}.
	 * If {@link Issue issue} is {@literal null}, then the unassigned Avatar metadata is returned.
	 */
	private AvatarMetaData getAvatarMetaData(ApplicationUser user, @Nullable Issue issue) {
		String displayName;
		ApplicationUser assignee;
		if (issue == null) {
			assignee = null;
			displayName = null;
		} else {
			assignee = issue.getAssignee();
			displayName = assignee == null ? issue.getAssigneeId() : assignee.getDisplayName();
		}
		return new AvatarMetaData(displayName, avatarService.getAvatarURL(user, assignee, Avatar.Size.NORMAL).toString());
	}

}
