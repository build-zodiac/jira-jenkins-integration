/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.indexing;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.JobService;

import com.atlassian.jira.index.IssueSearchExtractor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;
import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toSet;

/**
 * {@link IssueSearchExtractor} specific to add Jenkins builds fields to the Issue Index
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class JenkinsIssueSearchExtractor implements IssueSearchExtractor {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsIssueSearchExtractor.class);
	private static final String ISSUE_PROPERTY_FIELD_PREFIX = "ISSUEPROP_jenkins$";
	private static final String ISSUE_PROPERTY_FIELD_SUFFIX = "builds";
	static final String WORST_RESULT_FIELD_NAME = ISSUE_PROPERTY_FIELD_PREFIX + "worstresult";
	private final ConfigurationService pluginConfiguration;
	private final JobService jobService;
	private final BuildService buildService;
	private final DoubleConverter doubleConverter;

	public JenkinsIssueSearchExtractor(ConfigurationService pluginConfiguration, JobService jobService, BuildService buildService,
	                                   @ComponentImport DoubleConverter doubleConverter) {
		this.pluginConfiguration = pluginConfiguration;
		this.jobService = jobService;
		this.buildService = buildService;
		this.doubleConverter = doubleConverter;
	}

	static String fieldNameForResult(Result result) {
		return ISSUE_PROPERTY_FIELD_PREFIX + result.key() + ISSUE_PROPERTY_FIELD_SUFFIX;
	}

	@Override
	@SuppressWarnings("ConstantConditions")
	public Set<String> indexEntity(Context<Issue> context, Document document) {
		long startTime = System.currentTimeMillis();
		final Set<String> fields = new HashSet<>();
		Issue issue = context.getEntity();
		Set<Build> buildsForIssue = buildService.getByIssueKey(issue.getKey());
		if (!buildsForIssue.isEmpty()) {
			// Add fields by build results
			Map<Result, Set<Build>> buildsByResult = buildsForIssue.stream().collect(groupingBy(Build::getResult, toSet()));
			Arrays.stream(Result.values())
			      .map(result -> createBuildResultField(result, buildsByResult.getOrDefault(result, emptySet())))
			      .peek(document::add)
			      .map(Fieldable::name)
			      .collect(toCollection(() -> fields));
			// Add the worst build state to the search index
			Map<Job, Set<Build>> buildsByJobId = buildsForIssue.stream().collect(groupingBy(Build::getJob, toSet()));
			final AtomicReference<Result> worst = new AtomicReference<>(Result.UNKNOWN);
			buildsByJobId.forEach((job, builds) -> {
				Optional<Build> build = empty();
				if (pluginConfiguration.useJobLatestBuildResultInIndexes()) {
					build = buildService.get(job, job.getLastBuild());
				}
				if (!build.isPresent()) {
					build = builds.stream()
					              .sorted(comparing(Build::getNumber, reverseOrder()))
					              .findFirst();
				}
				build.map(Build::getResult)
				     .filter(result -> result.isWorseThan(worst.get()))
				     .ifPresent(worst::set);
			});
			document.add(createField(WORST_RESULT_FIELD_NAME, worst.get().key()));
			fields.add(WORST_RESULT_FIELD_NAME);
			LOGGER.debug("Indexing {} took {} ms", issue.getKey(), System.currentTimeMillis() - startTime);
		}
		return unmodifiableSet(fields);
	}

	private Fieldable createBuildResultField(Result result, Set<Build> builds) {
		return createField(fieldNameForResult(result), doubleConverter.getStringForLucene(Double.valueOf(builds.size())));
	}

	private Field createField(String fieldName, String fieldValueAsString) {
		return new Field(fieldName, false, fieldValueAsString, Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS, Field.TermVector.NO);
	}

}
