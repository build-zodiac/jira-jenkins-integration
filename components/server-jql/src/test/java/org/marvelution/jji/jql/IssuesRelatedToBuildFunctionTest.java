/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.jql;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import static java.util.Arrays.stream;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link IssuesRelatedToBuildFunction}
 *
 * @author Mark Rekveld
 * @since 2.3.0
 */
public class IssuesRelatedToBuildFunctionTest extends TestSupport {

	@Mock
	private ApplicationUser user;
	@Mock
	private QueryCreationContext queryCreationContext;
	@Mock
	private TerminalClause terminalClause;
	@Mock
	private JobService jobService;
	@Mock
	private BuildService buildService;
	@Mock
	private BuildLinkService buildLinkService;
	@Mock
	private I18nHelper i18nHelper;
	private IssuesRelatedToBuildFunction function;

	@Before
	public void setUp() throws Exception {
		Answer<String> i18n = invocation -> stream(invocation.getArguments())
				.map(String::valueOf)
				.collect(joining("."));
		when(i18nHelper.getText(anyString())).then(i18n);
		when(i18nHelper.getText(anyString(), anyObject())).then(i18n);
		when(i18nHelper.getText(anyString(), anyInt())).then(i18n);
		function = new IssuesRelatedToBuildFunction(jobService, buildService, buildLinkService) {
			@Override
			protected I18nHelper getI18n() {
				return i18nHelper;
			}
		};
	}

	private FunctionOperand newFunctionOperand(String... arguments) {
		return new FunctionOperand("issuesRelatedToBuild", arguments);
	}

	@Test
	public void testValidate_BuildRange() throws Exception {
		when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1").setName("job")));
		when(jobService.find("unknown")).thenReturn(new ArrayList<>());

		// Verify an unknown job
		MessageSet messages = function.validate(user, newFunctionOperand("unknown", "1", "10"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.unknown.job.unknown"));

		// Verify a know job with a valid build range
		messages = function.validate(user, newFunctionOperand("job", "1", "10"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(0));

		// Verify a know job with a invalid build range start
		messages = function.validate(user, newFunctionOperand("job", "-1", "10"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
		messages = function.validate(user, newFunctionOperand("job", "nan", "10"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(2));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.nan"));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));

		// Verify a know job with a invalid build range end
		messages = function.validate(user, newFunctionOperand("job", "1", "-10"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));
		messages = function.validate(user, newFunctionOperand("job", "1", "nan"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(2));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.nan"));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));

		// Verify a know job with a build range end before start
		messages = function.validate(user, newFunctionOperand("job", "10", "1"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.range"));

		verify(jobService, times(1)).find("unknown");
		verify(jobService, times(6)).find("job");
		verifyNoMoreInteractions(jobService, buildService);
	}

	@Test
	public void testValidate_BuildByNumber() throws Exception {
		when(jobService.find("job")).thenReturn(singletonList(new Job().setId("1").setName("job")));
		when(jobService.find("unknown")).thenReturn(new ArrayList<>());

		// Verify an unknown job
		MessageSet messages = function.validate(user, newFunctionOperand("unknown", "1"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.unknown.job.unknown"));

		// Verify a know job with a valid build
		messages = function.validate(user, newFunctionOperand("job", "1"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(0));

		// Verify a know job with lastBuild
		messages = function.validate(user, newFunctionOperand("job", "lastBuild"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(0));

		// Verify a know job with a invalid build
		messages = function.validate(user, newFunctionOperand("job", "-1"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.-1"));
		messages = function.validate(user, newFunctionOperand("job", "nan"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.build.number.nan"));

		verify(jobService, times(1)).find("unknown");
		verify(jobService, times(4)).find("job");
		verifyNoMoreInteractions(jobService, buildService);
	}

	@Test
	public void testValidate_BuildById() throws Exception {
		when(buildService.get("1")).thenReturn(Optional.of(new Build().setId("1").setJob(new Job().setId("1")).setNumber(1)));

		// Verify an unknown build id
		MessageSet messages = function.validate(user, newFunctionOperand("2"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.unknown.build.2"));

		// Verify a valid build id
		messages = function.validate(user, newFunctionOperand("1"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(0));

		verify(buildService).get("1");
		verify(buildService).get("2");
		verifyNoMoreInteractions(jobService, buildService);
	}

	@Test
	public void testValidate_InvalidArgumentCount() throws Exception {
		// Verify no arguments
		MessageSet messages = function.validate(user, newFunctionOperand(), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.number.of.arguments.1"));

		// Verify to many arguments
		messages = function.validate(user, newFunctionOperand("1", "2", "3", "4"), terminalClause);
		assertThat(messages.getErrorMessages(), hasSize(1));
		assertThat(messages.getErrorMessages(), hasItem("jql.invalid.number.of.arguments.1"));

		verifyZeroInteractions(jobService, buildService);
	}

	@Test
	public void testGetValues_BuildRange() throws Exception {
		Job job = new Job().setId("1").setName("job");
		when(jobService.find("job")).thenReturn(singletonList(job));
		Set<Build> builds = of(1, 2, 3, 4).map(number -> new Build().setId("number")
		                                                            .setJob(job)
		                                                            .setNumber(number))
		                                  .collect(toSet());
		when(buildService.getAllInRange(eq(job), eq(1), eq(10))).thenReturn(builds);
		when(buildLinkService.getRelatedIssueKeys(any(Build.class))).then(invocation -> {
			int number = invocation.getArgumentAt(0, Build.class).getNumber();
			return of(number, number * 10).map(n -> "ISSUE-" + n).collect(toSet());
		});

		FunctionOperand functionOperand = newFunctionOperand("job", "1", "10");
		List<QueryLiteral> values = function.getValues(queryCreationContext, functionOperand, terminalClause);
		assertThat(values, hasSize(builds.size() * 2));
		builds.forEach(build -> {
			assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-" + build.getNumber())));
			assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-" + (build.getNumber() * 10))));
		});

		verify(jobService).find("job");
		verify(buildService).getAllInRange(eq(job), eq(1), eq(10));
		verify(buildLinkService, times(4)).getRelatedIssueKeys(any(Build.class));
		verifyNoMoreInteractions(buildService, jobService);
	}

	@Test
	public void testGetValues_BuildByNumber() throws Exception {
		Job job = new Job().setId("1").setName("job").setLastBuild(10);
		when(jobService.find("job")).thenReturn(singletonList(job));
		when(buildService.get(eq(job), anyInt())).then(invocation -> {
			int number = invocation.getArgumentAt(1, Integer.class);
			return Optional.of(new Build().setId("number").setJob(job).setNumber(number));
		});
		when(buildLinkService.getRelatedIssueKeys(any(Build.class))).then(invocation -> {
			int number = invocation.getArgumentAt(0, Build.class).getNumber();
			return of(number, number * 10).map(n -> "ISSUE-" + n).collect(toSet());
		});

		FunctionOperand functionOperand = newFunctionOperand("job", "1");
		List<QueryLiteral> values = function.getValues(queryCreationContext, functionOperand, terminalClause);
		assertThat(values, hasSize(2));
		assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-1")));
		assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-10")));

		functionOperand = newFunctionOperand("job", "lastBuild");
		values = function.getValues(queryCreationContext, functionOperand, terminalClause);
		assertThat(values, hasSize(2));
		assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-10")));
		assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-100")));

		verify(jobService, times(2)).find("job");
		verify(buildService).get(job, 1);
		verify(buildService).get(job, 10);
		verify(buildLinkService, times(2)).getRelatedIssueKeys(any(Build.class));
		verifyNoMoreInteractions(buildService, jobService);
	}

	@Test
	public void testGetValues_BuildById() throws Exception {
		Build build = new Build().setId("1").setJob(new Job().setId("1")).setNumber(1);
		when(buildService.get("1")).thenReturn(Optional.of(build));
		when(buildLinkService.getRelatedIssueKeys(build)).thenReturn(singleton("ISSUE-1"));

		FunctionOperand functionOperand = newFunctionOperand("1");
		List<QueryLiteral> values = function.getValues(queryCreationContext, functionOperand, terminalClause);
		assertThat(values, hasSize(1));
		assertThat(values, hasItem(new QueryLiteral(functionOperand, "ISSUE-1")));

		verify(buildService).get("1");
		verify(buildLinkService).getRelatedIssueKeys(build);
		verifyNoMoreInteractions(buildService, jobService);
	}

}
