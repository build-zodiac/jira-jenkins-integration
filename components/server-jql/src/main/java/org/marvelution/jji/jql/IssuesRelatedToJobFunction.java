/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.jql;

import java.util.List;
import javax.annotation.Nonnull;

import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

/**
 * JQL Function to located issues related to a Jenkins Job
 *
 * @author Mark Rekveld
 * @since 1.2.1
 */
public class IssuesRelatedToJobFunction extends AbstractJqlFunction {

	private final JobService jobService;
	private final BuildService buildService;
	private final BuildLinkService buildLinkService;

	public IssuesRelatedToJobFunction(JobService jobService, BuildService buildService, BuildLinkService buildLinkService) {
		this.jobService = jobService;
		this.buildService = buildService;
		this.buildLinkService = buildLinkService;
	}

	@Override
	@Nonnull
	public MessageSet validate(ApplicationUser user, @Nonnull FunctionOperand functionOperand, @Nonnull TerminalClause terminalClause) {
		MessageSet messages = new MessageSetImpl();
		final List<String> arguments = functionOperand.getArgs();
		if (arguments.size() != 1) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.number.of.arguments", 1));
		} else {
			if (jobService.find(arguments.get(0)).isEmpty()) {
				messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
			}
		}
		return messages;
	}

	@Override
	@Nonnull
	public List<QueryLiteral> getValues(@Nonnull QueryCreationContext queryCreationContext, @Nonnull final FunctionOperand functionOperand,
	                                    @Nonnull TerminalClause terminalClause) {
		return unmodifiableList(jobService.find(functionOperand.getArgs().get(0)).stream()
		                                  .flatMap(job -> buildService.getByJob(job).stream())
		                                  .flatMap(build -> buildLinkService.getRelatedIssueKeys(build).stream())
		                                  .map(issueKey -> new QueryLiteral(functionOperand, issueKey))
		                                  .collect(toList()));
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	@Override
	@Nonnull
	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

}
