/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.Map;
import java.util.function.Function;
import javax.annotation.Nullable;

import org.marvelution.jji.data.access.api.BaseDAO;
import org.marvelution.jji.data.access.model.v2.Entity;
import org.marvelution.jji.model.HasId;

import com.atlassian.activeobjects.external.ActiveObjects;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
abstract class AbstractModelDAO<M extends HasId, E extends Entity> extends AbstractActiveObjectsDAO<E> implements BaseDAO<M> {

	final Function<E, M> transformer;

	AbstractModelDAO(ActiveObjects activeObjects, Class<E> entityClass, Function<E, M> transformer) {
		super(activeObjects, entityClass);
		this.transformer = transformer;
	}

	@Override
	@Nullable
	public M get(String id) {
		return getOne(id)
				.map(transformer)
				.orElse(null);
	}

	@Override
	public M save(M model) {
		E entity;
		if (model.getId() == null) {
			entity = insert(toInsertMap(model));
		} else {
			entity = getOne(model.getId()).get();
			updateEntity(entity, model);
			entity.save();
		}
		afterSave(model, entity);
		return transformer.apply(entity);
	}

	/**
	 * Transform the specified {@link M model} to a {@link Map} to use to insert it in the database.
	 */
	abstract NullCharacterRemovingHashMap toInsertMap(M model);

	/**
	 * Update the specified {@link E entity} using the {@link M model} to update it in the database.
	 */
	abstract void updateEntity(E entity, M model);

	/**
	 * Called after the specified {@link M model} is saved, resulting in the specified {@link E entity}.
	 */
	void afterSave(M model, E entity) {
	}

	@Override
	public void delete(String id) {
		deleteOne(id);
	}

}
