/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.JobDAO;
import org.marvelution.jji.data.access.model.v2.JobEntity;
import org.marvelution.jji.model.Job;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;

import static org.marvelution.jji.data.access.EntityHelper.deleteJob;
import static org.marvelution.jji.data.access.model.v2.JobEntity.DELETED;
import static org.marvelution.jji.data.access.model.v2.JobEntity.DESCRIPTION;
import static org.marvelution.jji.data.access.model.v2.JobEntity.DISPLAY_NAME;
import static org.marvelution.jji.data.access.model.v2.JobEntity.LAST_BUILD;
import static org.marvelution.jji.data.access.model.v2.JobEntity.LINKED;
import static org.marvelution.jji.data.access.model.v2.JobEntity.NAME;
import static org.marvelution.jji.data.access.model.v2.JobEntity.OLDEST_BUILD;
import static org.marvelution.jji.data.access.model.v2.JobEntity.SITE_ID;
import static org.marvelution.jji.data.access.model.v2.JobEntity.URL_NAME;

import static java.util.stream.Collectors.toList;
import static net.java.ao.Query.select;

/**
 * The Default implementation of the {@link JobDAO} interface
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultJobDAO extends AbstractModelDAO<Job, JobEntity> implements JobDAO {

	@Inject
	public DefaultJobDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, JobEntity.class, TransformerHelper::entityToJob);
	}

	@Override
	public List<Job> getAllBySiteId(String siteId, boolean includeDeleted) {
		Query query = select();
		if (includeDeleted) {
			query.where(SITE_ID + " = ?", siteId);
		} else {
			query.where(SITE_ID + " = ? AND " + DELETED + " = ?", siteId, Boolean.FALSE);
		}
		query.order(NAME);
		return findAndTransform(query);
	}

	@Override
	public List<Job> getAll(boolean includeDeleted) {
		Query query = select();
		if (!includeDeleted) {
			query.where(DELETED + " = ?", Boolean.FALSE);
		}
		query.order(NAME);
		return findAndTransform(query);
	}

	@Override
	public List<Job> find(String name) {
		return findAndTransform(select().where(NAME + " = ? OR " + DISPLAY_NAME + " = ?", name, name));
	}

	private List<Job> findAndTransform(Query query) {
		return find(query)
				.map(transformer)
				.collect(toList());
	}

	@Override
	NullCharacterRemovingHashMap toInsertMap(Job job) {
		NullCharacterRemovingHashMap entity = new NullCharacterRemovingHashMap();
		entity.put(SITE_ID, job.getSite().getId());
		entity.put(NAME, job.getName());
		entity.put(URL_NAME, job.getUrlNameOrNull());
		entity.put(DISPLAY_NAME, job.getDisplayNameOrNull());
		entity.put(DESCRIPTION, job.getDescription());
		entity.put(LAST_BUILD, job.getLastBuild());
		entity.put(OLDEST_BUILD, job.getOldestBuild());
		entity.put(LINKED, job.isLinked());
		entity.put(DELETED, job.isDeleted());
		return entity;
	}

	@Override
	void updateEntity(JobEntity entity, Job job) {
		entity.setName(job.getName());
		entity.setUrlName(job.getUrlNameOrNull());
		entity.setDisplayName(job.getDisplayNameOrNull());
		entity.setDescription(job.getDescription());
		entity.setLastBuild(job.getLastBuild());
		entity.setOldestBuild(job.getOldestBuild());
		entity.setLinked(job.isLinked());
		entity.setDeleted(job.isDeleted());
	}

	@Override
	void delete(JobEntity entity) {
		deleteJob(entity, this);
	}
}
