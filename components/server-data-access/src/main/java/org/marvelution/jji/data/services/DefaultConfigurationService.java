/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.TextResolver;
import org.marvelution.jji.data.services.api.exceptions.ValidationException;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.ErrorMessages;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Default {@link ConfigurationService} implementation
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@Named
@ExportAsDevService(ConfigurationService.class)
public class DefaultConfigurationService implements ConfigurationService {

	public static final String SETTING_PREFIX = "jji.";
	static final String MAX_BUILDS_PER_PAGE = SETTING_PREFIX + "max_builds_per_page";
	static final String JIRA_BASE_RPC_URL = SETTING_PREFIX + "jira_base_rpc_url";
	static final String USE_JOB_LATEST_BUILD_WHEN_INDEXING = SETTING_PREFIX + "use_job_latest_build_when_indexing";
	private final TextResolver textResolver;
	private final ApplicationProperties applicationProperties;
	private final PluginSettingsFactory pluginSettingsFactory;
	private transient PluginSettings pluginSettings;

	@Inject
	public DefaultConfigurationService(TextResolver textResolver, @ComponentImport ApplicationProperties applicationProperties,
	                                   @ComponentImport PluginSettingsFactory pluginSettingsFactory) {
		this.textResolver = textResolver;
		this.applicationProperties = applicationProperties;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	public static PluginSettings getPluginSettings(PluginSettingsFactory pluginSettingsFactory) {
		return pluginSettingsFactory.createGlobalSettings();
	}

	@Override
	public String getJIRAInstanceName() {
		return applicationProperties.getDisplayName();
	}

	@Override
	public URI getJIRABaseUrl() {
		return URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL));
	}

	@Override
	public URI getJIRABaseRpcUrl() {
		String uri = (String) getPluginSettings().get(JIRA_BASE_RPC_URL);
		return isNotBlank(uri) ? URI.create(uri) : getJIRABaseUrl();
	}

	@Override
	public int getMaximumBuildsPerPage() {
		Integer count;
		try {
			count = Integer.parseInt((String) getPluginSettings().get(MAX_BUILDS_PER_PAGE));
		} catch (Exception e) {
			count = DEFAULT_MAX_BUILDS_PER_PAGE;
		}
		return count;
	}

	@Override
	public boolean useJobLatestBuildResultInIndexes() {
		return Boolean.valueOf((String) getPluginSettings().get(USE_JOB_LATEST_BUILD_WHEN_INDEXING));
	}

	@Override
	public void saveConfiguration(Configuration configuration) {
		ErrorMessages errorMessages = new ErrorMessages();
		if (configuration.getJIRABaseRpcUrl() != null && configuration.getJIRABaseRpcUrl().getHost() == null) {
			errorMessages.addError("jiraBaseRpcUrl", textResolver.getText("url.invalid", "host"));
		}
		if (!errorMessages.hasErrors()) {
			setJIRABaseRpcUrl(configuration.getJIRABaseRpcUrl());
			setMaximumBuildsPerPage(configuration.getMaxBuildsPerPage());
			setUseJobLatestBuildResultInIndexes(configuration.isUseJobLatestBuildWhenIndexing());
		} else {
			throw new ValidationException(errorMessages);
		}
	}

	private void setJIRABaseRpcUrl(URI rpcUrl) {
		if (rpcUrl == null || rpcUrl.equals(getJIRABaseUrl())) {
			getPluginSettings().remove(JIRA_BASE_RPC_URL);
		} else {
			getPluginSettings().put(JIRA_BASE_RPC_URL, rpcUrl.toASCIIString());
		}
	}

	private void setMaximumBuildsPerPage(int maximumBuildsPerPage) {
		if (maximumBuildsPerPage < 1 && maximumBuildsPerPage != -1) {
			getPluginSettings().remove(MAX_BUILDS_PER_PAGE);
		} else {
			getPluginSettings().put(MAX_BUILDS_PER_PAGE, Integer.toString(maximumBuildsPerPage));
		}
	}

	private void setUseJobLatestBuildResultInIndexes(boolean useJobLatestBuildResult) {
		getPluginSettings().put(USE_JOB_LATEST_BUILD_WHEN_INDEXING, Boolean.toString(useJobLatestBuildResult));
	}

	private PluginSettings getPluginSettings() {
		if (pluginSettings == null) {
			pluginSettings = getPluginSettings(pluginSettingsFactory);
		}
		return pluginSettings;
	}
}
