/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.model.Site;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseConnectTimeoutException;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseReadTimeoutException;
import org.codehaus.jackson.map.ObjectMapper;

import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.ws.rs.core.UriBuilder.fromUri;

/**
 * JSON Rest Implementation of the {@link SiteClient}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(SiteClient.class)
public class DefaultSiteClient extends BaseSiteClient {

	private static final int CALL_TIMEOUT = 50000;
	private final NonMarshallingRequestFactory<?> requestFactory;

	@Inject
	public DefaultSiteClient(ConfigurationService configurationService, @ComponentImport NonMarshallingRequestFactory<?> requestFactory) {
		super(configurationService);
		this.requestFactory = requestFactory;
	}

	@Override
	protected String getJiraRpcUrl() {
		return fromUri(configurationService.getJIRABaseRpcUrl()).path("rest").path("jenkins").path("latest").build().toASCIIString();
	}

	@Override
	protected RestResponse get(Site site, URI uri) {
		return executeRequest(uri, createRequest(site, Request.MethodType.GET, uri));
	}

	@Override
	protected RestResponse post(Site site, URI uri, Map<String, Object> json) {
		Request<?, ?> request = createRequest(site, Request.MethodType.POST, uri);
		handleCrumbIfRequired(site, request::addHeader);
		try {
			request.setRequestBody(new ObjectMapper().writeValueAsString(json), MediaType.APPLICATION_JSON);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		return executeRequest(uri, request);
	}

	private Request<?, ?> createRequest(Site site, Request.MethodType methodType, URI uri) {
		Request<?, ?> request = requestFactory.createRequest(methodType, uri.toASCIIString());
		if (site.isSecured()) {
			request.addBasicAuthentication(uri.getHost(), site.getUser(), site.getToken());
		}
		request.setConnectionTimeout(CALL_TIMEOUT);
		request.setSoTimeout(CALL_TIMEOUT);
		return request;
	}

	private <R extends Response> RestResponse executeRequest(URI uri, Request<?, R> request) {
		RestResponse response;
		try {
			response = request.executeAndReturn(RestResponse::new);
		} catch (ResponseConnectTimeoutException | ResponseReadTimeoutException e) {
			logger.warn("Connection timeout occurred on {}", uri, e);
			response = new RestResponse("Unable to connect to Jenkins. Connection timed out.");
		} catch (ResponseException e) {
			logger.error("Failed to connect to {}; {}", uri, e.getMessage(), e);
			response = new RestResponse("Failed to execute request: " + e.getMessage());
		}
		if (response.hasAuthErrors()) {
			logger.error("Authentication failure on {}", uri);
		}
		return response;
	}

	static class RestResponse extends SiteResponse {

		private final Optional<Response> response;

		RestResponse(Response response) {
			this.response = of(response);
		}

		RestResponse(String... errors) {
			response = empty();
			this.errors.addAll(asList(errors));
		}

		@Override
		public boolean isSuccessful() {
			return response.map(Response::isSuccessful).orElse(false) && !hasAuthErrors() && !hasErrors();
		}

		@Override
		public int getStatusCode() {
			return response.map(Response::getStatusCode).orElse(-1);
		}

		@Override
		public String getStatusMessage() {
			return response.map(Response::getStatusText).orElse(null);
		}

		@Override
		protected InputStream getResponseBodyAsStream() throws IOException {
			try {
				return response.orElseThrow(() -> new IllegalStateException("No response present.")).getResponseBodyAsStream();
			} catch (ResponseException e) {
				throw new IOException(e);
			}
		}
	}

}
