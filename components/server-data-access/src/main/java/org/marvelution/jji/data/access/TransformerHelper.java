/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.ArrayList;
import java.util.List;

import org.marvelution.jji.data.access.model.v2.BuildEntity;
import org.marvelution.jji.data.access.model.v2.ChangeSetEntity;
import org.marvelution.jji.data.access.model.v2.JobEntity;
import org.marvelution.jji.data.access.model.v2.SiteEntity;
import org.marvelution.jji.data.access.model.v2.TestResultsEntity;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.ChangeSet;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.TestResults;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
class TransformerHelper {

	static Site entityToSite(SiteEntity entity) {
		return new Site()
				.setId(entity.getID())
				.setName(entity.getName())
				.setSharedSecret(entity.getSharedSecret())
				.setType(entity.getSiteType())
				.setRpcUrl(entity.getRpcUrl())
				.setDisplayUrl(entity.getDisplayUrl())
				.setJenkinsPluginInstalled(entity.isJenkinsPluginInstalled())
				.setAutoLinkNewJobs(entity.isAutoLinkNewJobs())
				.setUseCrumbs(entity.isUseCrumbs())
				.setUser(entity.getUser())
				.setToken(entity.getUserToken());
	}

	static Job entityToJob(JobEntity entity) {
		return new Job()
				.setId(entity.getID())
				.setSite(entityToSite(entity.getSite()))
				.setName(entity.getName())
				.setUrlName(entity.getUrlName())
				.setDisplayName(entity.getDisplayName())
				.setDescription(entity.getDescription())
				.setLastBuild(entity.getLastBuild())
				.setOldestBuild(entity.getOldestBuild())
				.setLinked(entity.isLinked())
				.setDeleted(entity.isDeleted());
	}

	static Build entityToBuild(BuildEntity entity) {
		return new Build()
				.setId(entity.getID())
				.setJob(entityToJob(entity.getJob()))
				.setNumber(entity.getBuildNumber())
				.setDisplayName(entity.getDisplayName())
				.setDescription(entity.getDescription())
				.setCause(entity.getCause())
				.setDuration(entity.getDuration())
				.setTimestamp(entity.getTimeStamp())
				.setResult(entity.getResult())
				.setBuiltOn(entity.getBuiltOn())
				.setDeleted(entity.isDeleted())
				.setChangeSet(entitiesToChangeSets(entity.getChangeSets()))
				.setTestResults(ofNullable(entity.getTestResults())
						                .map(TransformerHelper::entityToTestResults)
						                .orElse(null));
	}

	private static List<ChangeSet> entitiesToChangeSets(ChangeSetEntity[] changeSetEntities) {
		if (changeSetEntities == null || changeSetEntities.length == 0) {
			return new ArrayList<>();
		} else {
			return stream(changeSetEntities).map(TransformerHelper::entityToChangeSet)
			                                .collect(toList());
		}
	}

	private static ChangeSet entityToChangeSet(ChangeSetEntity entity) {
		return new ChangeSet().setCommitId(entity.getCommitId())
		                      .setMessage(entity.getCommitMessage());
	}

	private static TestResults entityToTestResults(TestResultsEntity entity) {
		return new TestResults()
				.setSkipped(entity.getSkipped())
				.setFailed(entity.getFailed())
				.setTotal(entity.getTotal());
	}


}
