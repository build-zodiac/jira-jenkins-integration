/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v1;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Build Mappings
 *
 * @author Mark Rekveld
 * @since 1.0.0
 * @deprecated use model v2 instead
 */
@Deprecated
@Preload
public interface BuildMapping extends Entity {

	String JOB_ID = "JOB_ID";
	String BUILD_NUMBER = "BUILD_NUMBER";
	String DISPLAY_NAME = "DISPLAY_NAME";
	String CAUSE = "CAUSE";
	String DELETED = "DELETED";
	String DURATION = "DURATION";
	String TIME_STAMP = "TIME_STAMP";
	String RESULT = "RESULT";
	String BUILT_ON = "BUILT_ON";

	int getJobId();

	void setJobId(int jobId);

	int getBuildNumber();

	void setBuildNumber(int buildNumber);

	/**
	 * @since 1.5.0
	 */
	String getDisplayName();

	/**
	 * @since 1.5.0
	 */
	void setDisplayName(String displayName);

	String getCause();

	void setCause(String cause);

	boolean isDeleted();

	void setDeleted(boolean deleted);

	long getDuration();

	void setDuration(long duration);

	long getTimeStamp();

	void setTimeStamp(long timestamp);

	String getResult();

	void setResult(String result);

	String getBuiltOn();

	void setBuiltOn(String builtOn);

}
