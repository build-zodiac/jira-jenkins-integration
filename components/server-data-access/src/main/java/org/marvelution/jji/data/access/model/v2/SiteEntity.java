/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import java.net.URI;
import javax.annotation.Nullable;

import org.marvelution.jji.model.SiteType;

import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import static net.java.ao.schema.StringLength.MAX_LENGTH;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Preload
@Table("Sites")
public interface SiteEntity extends Entity {

	String NAME = "NAME";
	String SHARED_SECRET = "SHARED_SECRET";
	String SITE_TYPE = "SITE_TYPE";
	String RPC_URL = "RPC_URL";
	String DISPLAY_URL = "DISPLAY_URL";
	String USER = "USER";
	String USER_TOKEN = "USER_TOKEN";
	String AUTO_LINK_NEW_JOBS = "AUTO_LINK_NEW_JOBS";
	String JENKINS_PLUGIN_INSTALLED = "JENKINS_PLUGIN_INSTALLED";
	String USE_CRUMBS = "USE_CRUMBS";

	@NotNull
	String getName();

	void setName(String name);

	@NotNull
	@StringLength(MAX_LENGTH)
	String getSharedSecret();

	void setSharedSecret(String sharedSecret);

	@NotNull
	SiteType getSiteType();

	void setSiteType(SiteType siteType);

	@NotNull
	URI getRpcUrl();

	void setRpcUrl(URI rpcUrl);

	@Nullable
	URI getDisplayUrl();

	void setDisplayUrl(@Nullable URI displayUrl);

	@Nullable
	String getUser();

	void setUser(@Nullable String user);

	@Nullable
	String getUserToken();

	void setUserToken(@Nullable String userToken);

	boolean isUseCrumbs();

	void setUseCrumbs(boolean useCrumbs);

	boolean isAutoLinkNewJobs();

	void setAutoLinkNewJobs(boolean autoLinkNewJobs);

	boolean isJenkinsPluginInstalled();

	void setJenkinsPluginInstalled(boolean jenkinsPluginInstalled);

	@OneToMany(reverse = "getSite")
	JobEntity[] getJobs();

}
