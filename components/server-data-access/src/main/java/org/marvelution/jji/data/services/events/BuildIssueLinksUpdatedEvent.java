/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.events;

import java.util.Set;

/**
 * Event published to notify of updated links between a Build and one or more Issues
 *
 * @author Mark Rekveld
 * @since 2.1.1
 */
public class BuildIssueLinksUpdatedEvent {

	private final String buildId;
	private final Set<String> issueKeys;

	public BuildIssueLinksUpdatedEvent(String buildId, Set<String> issueKeys) {
		this.buildId = buildId;
		this.issueKeys = issueKeys;
	}

	public String getBuildId() {
		return buildId;
	}

	public Set<String> getIssueKeys() {
		return issueKeys;
	}

}
