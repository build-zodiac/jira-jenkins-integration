/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.BuildDAO;
import org.marvelution.jji.data.services.api.BuildService;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;

/**
 * Default {@link BuildService} implementation.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(BuildService.class)
public class DefaultBuildService extends BaseBuildService {

	@Inject
	public DefaultBuildService(BuildDAO buildDAO) {
		super(buildDAO);
	}
}
