/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nullable;

import org.marvelution.jji.data.access.model.v2.BuildEntity;
import org.marvelution.jji.data.access.model.v2.IssueLinkEntity;
import org.marvelution.jji.model.BuildIssueFilter;

import net.java.ao.Query;

import static org.marvelution.jji.data.access.model.v2.BuildEntity.TIME_STAMP;
import static org.marvelution.jji.data.access.model.v2.Entity.ID;
import static org.marvelution.jji.data.access.model.v2.IssueLinkEntity.BUILD_ID;
import static org.marvelution.jji.data.access.model.v2.IssueLinkEntity.ISSUE_KEY;
import static org.marvelution.jji.data.access.model.v2.IssueLinkEntity.PROJECT_KEY;

import static java.util.stream.Collectors.joining;

/**
 * BuildIssueFilter {@link Query} Builder.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BuildIssueFilterQueryBuilder {

	private static final String AND = " AND ";
	private static final String IN = " IN ";
	private static final String NOT_IN = " NOT" + IN;
	private static final String QUOTE_CHAR = "'";
	private static final String DESC = " DESC";
	private static final String BUILD_ALIAS = "build";
	private static final String BUILD_ALIAS_PREFIX = BUILD_ALIAS + ".";
	private static final String LINK_ALIAS = "link";
	private static final String LINK_ALIAS_PREFIX = LINK_ALIAS + ".";

	/**
	 * Build the {@link Query} from the {@link BuildIssueFilter} to select {@link BuildEntity BuildMappings}.
	 */
	@Nullable
	static Query toQuery(BuildIssueFilter filter) {
		StringBuilder projectClaus = new StringBuilder();
		StringBuilder issueClaus = new StringBuilder();
		if (filter != null) {
			if (filter.getInProjectKeys() != null && !filter.getInProjectKeys().isEmpty()) {
				projectClaus.append(LINK_ALIAS_PREFIX).append(PROJECT_KEY).append(IN)
				            .append(joinToSqlIn(filter.getInProjectKeys()));
			}
			if (filter.getNotInProjectKeys() != null && !filter.getNotInProjectKeys().isEmpty()) {
				if (projectClaus.length() != 0) {
					projectClaus.append(AND);
				}
				projectClaus.append(LINK_ALIAS_PREFIX).append(PROJECT_KEY).append(NOT_IN)
				            .append(joinToSqlIn(filter.getNotInProjectKeys()));
			}
			if (filter.getInIssueKeys() != null && !filter.getInIssueKeys().isEmpty()) {
				issueClaus.append(LINK_ALIAS_PREFIX).append(ISSUE_KEY).append(IN)
				          .append(joinToSqlIn(filter.getInIssueKeys()));
			}
			if (filter.getNotInIssueKeys() != null && !filter.getNotInIssueKeys().isEmpty()) {
				if (issueClaus.length() != 0) {
					issueClaus.append(AND);
				}
				issueClaus.append(LINK_ALIAS_PREFIX).append(ISSUE_KEY).append(NOT_IN)
				          .append(joinToSqlIn(filter.getNotInIssueKeys()));
			}
		}
		StringBuilder finalClaus = new StringBuilder();
		if (projectClaus.length() > 0) {
			finalClaus.append("(").append(projectClaus).append(")");
		}
		if (issueClaus.length() > 0) {
			if (finalClaus.length() > 0) {
				finalClaus.append(AND);
			}
			finalClaus.append("(").append(issueClaus).append(")");
		}
		if (finalClaus.length() == 0) {
			return null;
		} else {
			return Query.select()
			            .alias(BuildEntity.class, BUILD_ALIAS)
			            .alias(IssueLinkEntity.class, LINK_ALIAS)
			            .from(BuildEntity.class)
			            .join(IssueLinkEntity.class, BUILD_ALIAS_PREFIX + ID + " = " + LINK_ALIAS_PREFIX + BUILD_ID)
			            .where(finalClaus.toString())
			            .order(TIME_STAMP + DESC);
		}
	}

	/**
	 * Join the given {@link Set} of keys to be used in a SQL IN statement.
	 *
	 * @param keys the keys to join
	 * @return the SQL formatted join
	 */
	private static String joinToSqlIn(Iterable<?> keys) {
		List<String> parts = new ArrayList<>();
		for (Object key : keys) {
			if (key != null) {
				if (key instanceof String) {
					parts.add(QUOTE_CHAR + key + QUOTE_CHAR);
				} else if (key instanceof Boolean || key instanceof Number) {
					parts.add(String.valueOf(key));
				} else {
					parts.add(QUOTE_CHAR + key.toString() + QUOTE_CHAR);
				}
			}
		}
		return parts.stream().filter(Objects::nonNull).collect(joining(",", "(", ")"));
	}

}
