/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.BuildDAO;
import org.marvelution.jji.data.access.model.v2.BuildEntity;
import org.marvelution.jji.data.access.model.v2.ChangeSetEntity;
import org.marvelution.jji.data.access.model.v2.TestResultsEntity;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.BuildIssueFilter;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import net.java.ao.RawEntity;

import static org.marvelution.jji.data.access.BuildIssueFilterQueryBuilder.toQuery;
import static org.marvelution.jji.data.access.EntityHelper.deleteBuild;
import static org.marvelution.jji.data.access.model.v2.BuildEntity.*;

import static java.util.stream.Collectors.toSet;
import static net.java.ao.Query.select;

/**
 * Default {@link BuildDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultBuildDAO extends AbstractModelDAO<Build, BuildEntity> implements BuildDAO {

	@Inject
	public DefaultBuildDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, BuildEntity.class, TransformerHelper::entityToBuild);
	}

	@Override
	public Build get(String jobId, int buildNumber) {
		return findOne(select().where(JOB_ID + " =  ? AND " + BUILD_NUMBER + " = ?", jobId, buildNumber))
				.map(transformer)
				.orElse(null);
	}

	@Override
	public Set<Build> getAllInRange(String jobId, int start, int end) {
		Query query;
		if (end == -1) {
			query = select().where(JOB_ID + " = ? AND " + BUILD_NUMBER + " >= ?", jobId, start);
		} else {
			query = select().where(JOB_ID + " = ? AND " + BUILD_NUMBER + " >= ? AND " + BUILD_NUMBER + " <= ?", jobId, start, end);
		}
		return find(query.order(TIME_STAMP))
				.map(transformer)
				.collect(toSet());
	}

	@Override
	public Set<Build> getAllByJob(String jobId) {
		return find(select().where(JOB_ID + " = ?", jobId).order(TIME_STAMP))
				.map(transformer)
				.collect(toSet());
	}

	@Override
	public Set<Build> getLatestByFilter(BuildIssueFilter filter, int maxResults) {
		Query query = toQuery(filter);
		if (query != null) {
			return find(query.limit(maxResults))
					.map(transformer)
					.collect(toSet());
		} else {
			return new HashSet<>();
		}
	}

	@Override
	NullCharacterRemovingHashMap toInsertMap(Build build) {
		NullCharacterRemovingHashMap entity = new NullCharacterRemovingHashMap();
		entity.put(JOB_ID, build.getJob().getId());
		entity.put(BUILD_NUMBER, build.getNumber());
		entity.put(DISPLAY_NAME, build.getDisplayName());
		entity.put(DESCRIPTION, build.getDescription());
		entity.put(CAUSE, build.getCause());
		entity.put(DELETED, build.isDeleted());
		entity.put(DURATION, build.getDuration());
		entity.put(TIME_STAMP, build.getTimestamp());
		entity.put(RESULT, build.getResult());
		entity.put(BUILT_ON, build.getBuiltOnOrNull());
		return entity;
	}

	@Override
	void updateEntity(BuildEntity entity, Build build) {
		entity.setBuildNumber(build.getNumber());
		entity.setDisplayName(build.getDisplayName());
		entity.setDescription(build.getDescription());
		entity.setCause(build.getCause());
		entity.setResult(build.getResult());
		entity.setDuration(build.getDuration());
		entity.setTimeStamp(build.getTimestamp());
		entity.setBuiltOn(build.getBuiltOnOrNull());
		entity.setDeleted(build.isDeleted());
	}

	@Override
	void afterSave(Build build, BuildEntity entity) {
		if (build.getTestResults() != null) {
			delete(TestResultsEntity.class, select().where(TestResultsEntity.BUILD_ID + " = ?", entity.getID()));
			NullCharacterRemovingHashMap m = new NullCharacterRemovingHashMap();
			m.put(TestResultsEntity.BUILD_ID, entity.getID());
			m.put(TestResultsEntity.SKIPPED, build.getTestResults().getSkipped());
			m.put(TestResultsEntity.FAILED, build.getTestResults().getFailed());
			m.put(TestResultsEntity.TOTAL, build.getTestResults().getTotal());
			insert(TestResultsEntity.class, m);
		}
		if (!build.getChangeSet().isEmpty()) {
			delete(ChangeSetEntity.class, select().where(ChangeSetEntity.BUILD_ID + " = ?", entity.getID()));
			build.getChangeSet().stream()
			     .filter(Objects::nonNull)
			     .map(changeSet -> {
				     NullCharacterRemovingHashMap m = new NullCharacterRemovingHashMap();
				     m.put(ChangeSetEntity.BUILD_ID, entity.getID());
				     m.put(ChangeSetEntity.COMMIT_ID, changeSet.getCommitId());
				     m.put(ChangeSetEntity.COMMIT_MESSAGE, changeSet.getMessage());
				     return m;
			     })
			     .forEach(m -> insert(ChangeSetEntity.class, m));
		}
	}

	@Override
	public void deleteAllByJob(String jobId) {
		delete(select().where(JOB_ID + " = ?", jobId));
	}

	@Override
	void delete(BuildEntity entity) {
		deleteBuild(entity, this);
	}

	@Override
	public void markAsDeleted(String buildId) {
		Build build = get(buildId);
		if (build != null) {
			build.setDeleted(true);
			save(build);
		}
	}

	@Override
	public void markAllInJobAsDeleted(String jobId, int buildNumber) {
		Query query = select();
		if (buildNumber > 0) {
			query.where(JOB_ID + " = ? AND " + BUILD_NUMBER + " < ?", jobId, buildNumber);
		} else {
			query.where(JOB_ID + " = ?", jobId);
		}
		find(query).peek(mapping -> mapping.setDeleted(true)).forEach(RawEntity::save);
	}

}
