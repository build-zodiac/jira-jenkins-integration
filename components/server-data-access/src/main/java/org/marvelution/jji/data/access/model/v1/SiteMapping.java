/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v1;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Site mappings
 *
 * @author Mark Rekveld
 * @since 1.0.0
 * @deprecated use model v2 instead
 */
@Deprecated
@Preload
public interface SiteMapping extends Entity {

	String NAME = "NAME";
	String AUTO_LINK = "AUTO_LINK";
	String SUPPORTS_BACK_LINK = "SUPPORTS_BACK_LINK";
	String USE_CRUMBS = "USE_CRUMBS";
	String SITE_TYPE = "SITE_TYPE";
	String RPC_URL = "RPC_URL";
	String DISPLAY_URL = "DISPLAY_URL";
	String USER = "USER";
	String USER_TOKEN = "USER_TOKEN";

	String getName();

	void setName(String name);

	boolean isAutoLink();

	void setAutoLink(boolean autoLink);

	boolean isSupportsBackLink();

	void setSupportsBackLink(boolean supportsBackLink);

	/**
	 * @since 1.6.0
	 */
	boolean isUseCrumbs();

	/**
	 * @since 1.6.0
	 */
	void setUseCrumbs(boolean useCrumbs);

	/**
	 * @since 1.6.0
	 */
	String getSiteType();

	/**
	 * @since 1.6.0
	 */
	void setSiteType(String siteType);

	/**
	 * @since 1.6.0
	 */
	String getRpcUrl();

	/**
	 * @since 1.6.0
	 */
	void setRpcUrl(String rpcUrl);

	/**
	 * @since 1.6.0
	 */
	String getDisplayUrl();

	/**
	 * @since 1.6.0
	 */
	void setDisplayUrl(String displayUrl);

	/**
	 * @since 1.6.0
	 */
	String getUser();

	/**
	 * @since 1.6.0
	 */
	void setUser(String user);

	/**
	 * @since 1.6.0
	 */
	String getUserToken();

	/**
	 * @since 1.6.0
	 */
	void setUserToken(String userToken);

}
