/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.Objects;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.IssueDAO;
import org.marvelution.jji.data.access.model.v2.IssueLinkEntity;
import org.marvelution.jji.model.IssueReference;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.marvelution.jji.data.access.model.v2.IssueLinkEntity.BUILD_ID;
import static org.marvelution.jji.data.access.model.v2.IssueLinkEntity.ISSUE_KEY;
import static org.marvelution.jji.data.access.model.v2.IssueLinkEntity.PROJECT_KEY;
import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;
import static net.java.ao.Query.select;

/**
 * Default {@link IssueDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultIssueDAO extends AbstractActiveObjectsDAO<IssueLinkEntity> implements IssueDAO {

	@Inject
	public DefaultIssueDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, IssueLinkEntity.class);
	}

	@Override
	public Set<String> getIssueKeysByBuild(String buildId) {
		return find(select().where(BUILD_ID + " = ?", buildId))
				.map(IssueLinkEntity::getIssueKey)
				.distinct()
				.collect(toSet());
	}

	@Override
	public Set<String> getProjectKeysByBuild(String buildId) {
		return find(select().where(BUILD_ID + " = ?", buildId))
				.map(IssueLinkEntity::getProjectKey)
				.distinct()
				.collect(toSet());
	}

	@Override
	public int getIssueLinkCount(String buildId) {
		return activeObjects.count(IssueLinkEntity.class, select().where(BUILD_ID + " = ?", buildId));
	}

	@Override
	public Set<IssueReference> getIssueLinks(String buildId) {
		return find(select().where(BUILD_ID + " = ?", buildId))
				.map(entity -> new IssueReference().setIssueKey(entity.getIssueKey()).setProjectKey(entity.getProjectKey()))
				.collect(toSet());
	}

	@Override
	public void link(String buildId, IssueReference issue) {
		requireNonNull(buildId, "buildId may not be null");
		requireNonNull(issue.getIssueKey(), "issueKey may not be null");
		findOne(select().where(BUILD_ID + " = ? AND " + ISSUE_KEY + " = ?", buildId, issue.getIssueKey()))
				.ifPresent(this::deleteEntity);
		NullCharacterRemovingHashMap data = new NullCharacterRemovingHashMap();
		data.put(BUILD_ID, buildId);
		data.put(ISSUE_KEY, issue.getIssueKey());
		data.put(PROJECT_KEY, ofNullable(issue.getProjectKey())
				.orElse(extractProjectKeyFromIssueKey(issue.getIssueKey())));
		insert(data);
	}

	@Override
	public void relink(String buildId, Set<IssueReference> issues) {
		deleteWithSQL(BUILD_ID + " = ?", buildId);
		if (issues != null && !issues.isEmpty()) {
			issues.stream()
			      .filter(Objects::nonNull)
			      .map(issue -> {
				      NullCharacterRemovingHashMap data = new NullCharacterRemovingHashMap();
				      data.put(BUILD_ID, buildId);
				      data.put(ISSUE_KEY, issue.getIssueKey());
				      data.put(PROJECT_KEY, ofNullable(issue.getProjectKey())
						      .orElse(extractProjectKeyFromIssueKey(issue.getIssueKey())));
				      return data;
			      })
			      .forEach(this::insert);
		}
	}

	@Override
	public void unlinkForIssueKey(String issueKey) {
		requireNonNull(issueKey, "issueKey may not be null");
		logger.debug("Unlinked issue {} from all known builds", issueKey);
		deleteWithSQL(ISSUE_KEY + " = ?", issueKey);
	}

	@Override
	public void unlinkForProjectKey(String projectKey) {
		requireNonNull(projectKey, "projectKey may not be null");
		logger.debug("Unlinked project {} from all known builds", projectKey);
		deleteWithSQL(PROJECT_KEY + " = ?", projectKey);
	}

	@Override
	void delete(IssueLinkEntity entity) {
		deleteEntity(entity);
	}
}
