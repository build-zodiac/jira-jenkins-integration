/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.project.Project;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link DefaultIssueReferenceProvider}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DefaultIssueReferenceProviderTest extends TestSupport {

	@Mock
	private IssueManager issueManager;
	@Mock
	private ConfigurationService configurationService;
	private DefaultIssueReferenceProvider referenceProvider;

	@Before
	public void setUp() throws Exception {
		when(configurationService.getJIRABaseUrl()).thenReturn(URI.create("https://jira.example.com"));
		referenceProvider = new DefaultIssueReferenceProvider(configurationService, issueManager);
	}

	@Test
	public void testGetIssueReference() throws Exception {
		mockIssue("DEV-1", true);

		Optional<IssueReference> reference = referenceProvider.getIssueReference("DEV-1");

		assertThat(reference.isPresent(), is(true));
		assertThat(reference.orElse(null), equalTo(new IssueReference().setIssueKey("DEV-1").setProjectKey("DEV")));
	}

	@Test
	public void testGetIssueReference_NoProject() throws Exception {
		mockIssue("DEV-1", false);

		Optional<IssueReference> reference = referenceProvider.getIssueReference("DEV-1");

		assertThat(reference.isPresent(), is(true));
		assertThat(reference.orElse(null), equalTo(new IssueReference().setIssueKey("DEV-1").setProjectKey("DEV")));
	}

	@Test
	public void testGetIssueReference_UnknownIssue() throws Exception {
		Optional<IssueReference> reference = referenceProvider.getIssueReference("DEV-1");

		assertThat(reference.isPresent(), is(false));
	}
	
	@Test
	public void testGetIssueReferences() throws Exception {
		mockIssue("DEV-1", false);
		mockIssue("DEV-2", true);

		Set<IssueReference> references = referenceProvider.getIssueReferences(Stream.of("DEV-1", "DEV-2", "DEV-200").collect(toSet()));

		assertThat(references, hasSize(2));
		assertThat(references, hasItem(equalTo(new IssueReference().setIssueKey("DEV-1").setProjectKey("DEV"))));
		assertThat(references, hasItem(equalTo(new IssueReference().setIssueKey("DEV-2").setProjectKey("DEV"))));
	}

	@Test
	public void testGetIssueReferences_UnknownIssues() throws Exception {
		Set<IssueReference> references = referenceProvider.getIssueReferences(Stream.of("DEV-1", "DEV-2", "DEV-200").collect(toSet()));

		assertThat(references, empty());
	}

	@Test
	public void testSetIssueUrl() throws Exception {
		IssueReference reference = new IssueReference().setIssueKey("DEV-2").setProjectKey("DEV");
		referenceProvider.setIssueUrl(reference);
		assertThat(reference, equalTo(new IssueReference().setIssueKey("DEV-2").setProjectKey("DEV")
		                                                  .setIssueUrl(URI.create("https://jira.example.com/browse/DEV-2"))));
	}

	private void mockIssue(String issueKey, boolean withProject) {
		MutableIssue issue = mock(MutableIssue.class);
		when(issue.getKey()).thenReturn(issueKey);
		if (withProject) {
			Project project = mock(Project.class);
			when(project.getKey()).thenReturn(extractProjectKeyFromIssueKey(issueKey));
			when(issue.getProjectObject()).thenReturn(project);
		}
		when(issueManager.getIssueObject(issueKey)).thenReturn(issue);
	}
}
