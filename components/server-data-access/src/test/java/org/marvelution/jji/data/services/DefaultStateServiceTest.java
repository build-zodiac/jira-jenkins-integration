/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Map;

import org.marvelution.testing.TestSupport;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.software.api.conditions.IsSoftwareProjectCondition;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link DefaultStateService}
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DefaultStateServiceTest extends TestSupport {

	@Mock
	private ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;
	@Mock
	private IsSoftwareProjectCondition isSoftwareProjectCondition;
	@Mock
	private ProjectManager projectManager;
	@Mock
	private IssueManager issueManager;
	@Mock
	private PluginRetrievalService pluginRetrievalService;
	@Mock
	private Plugin plugin;
	private DefaultStateService stateService;

	@Before
	@SuppressWarnings("unchecked")
	public void setUp() throws Exception {
		when(pluginRetrievalService.getPlugin()).thenReturn(plugin);
		when(projectManager.getProjectObjByKey(anyString())).then(invocation -> mockProject(invocation.getArgumentAt(0, String.class)));
		when(issueManager.getIssueObject(anyString())).then(invocation -> mockIssue(invocation.getArgumentAt(0, String.class)));
		Answer<Boolean> shouldDisplayAnswer = invocation -> {
			Map<String, Object> context = invocation.getArgumentAt(0, Map.class);
			Project project = (Project) context.get(ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT);
			switch (project.getKey()) {
				case "DEV":
					return true;
				default:
					return false;
			}
		};
		when(projectDevToolsIntegrationFeatureCondition.shouldDisplay(any(Map.class))).then(shouldDisplayAnswer);
		when(isSoftwareProjectCondition.shouldDisplay(any(Map.class))).then(shouldDisplayAnswer);

		stateService = new DefaultStateService(projectDevToolsIntegrationFeatureCondition, isSoftwareProjectCondition, projectManager,
		                                       issueManager, pluginRetrievalService);
	}

	@Test
	public void testGetAddonKey() throws Exception {
		when(plugin.getKey()).thenReturn("my-addon-test-key");

		assertThat(stateService.getAddonKey(), is("my-addon-test-key"));
	}

	@Test
	public void testIsAddonEnabled() throws Exception {
		when(plugin.getPluginState())
				.thenReturn(PluginState.DISABLED)
				.thenReturn(PluginState.ENABLED);

		assertThat(stateService.isAddonEnabled(), is(false));
		assertThat(stateService.isAddonEnabled(), is(true));
	}

	@Test
	public void testAreFeaturesEnabledForIssue_SoftwareIssue() throws Exception {
		assertThat(stateService.areFeaturesEnabledForIssue("DEV-2"), is(true));
	}

	@Test
	public void testAreFeaturesEnabledForIssue_ServiceDeskIssue() throws Exception {
		assertThat(stateService.areFeaturesEnabledForIssue("SD-1"), is(false));
	}

	@Test
	public void testAreFeaturesEnabledForIssue_BusinessIssue() throws Exception {
		assertThat(stateService.areFeaturesEnabledForIssue("BUS-1"), is(false));
	}

	@Test
	public void testAreFeaturesEnabledForProject_SoftwareProject() throws Exception {
		assertThat(stateService.areFeaturesEnabledForProject("DEV"), is(true));
	}

	@Test
	public void testAreFeaturesEnabledForProject_ServiceDeskProject() throws Exception {
		assertThat(stateService.areFeaturesEnabledForProject("SD"), is(false));
	}

	@Test
	public void testAreFeaturesEnabledForProject_BusinessProject() throws Exception {
		assertThat(stateService.areFeaturesEnabledForProject("BUS"), is(false));
	}

	@Test
	public void testAreFeaturesEnabledFor_NullValue() throws Exception {
		assertThat(stateService.areFeaturesEnabledFor(null), is(false));
	}

	@Test
	public void testAreFeaturesEnabledFor_SoftwareIssue() throws Exception {
		Issue issue = mockIssue("DEV-2");

		assertThat(stateService.areFeaturesEnabledFor(issue), is(true));
	}

	@Test
	public void testAreFeaturesEnabledFor_ServiceDeskIssue() throws Exception {
		Issue issue = mockIssue("SD-2");

		assertThat(stateService.areFeaturesEnabledFor(issue), is(false));
	}

	@Test
	public void testAreFeaturesEnabledFor_BusinessIssue() throws Exception {
		Issue issue = mockIssue("BUS-2");

		assertThat(stateService.areFeaturesEnabledFor(issue), is(false));
	}

	@Test
	public void testAreFeaturesEnabledFor_SoftwareProject() throws Exception {
		Project project = mockProject("DEV");

		assertThat(stateService.areFeaturesEnabledFor(project), is(true));
	}

	@Test
	public void testAreFeaturesEnabledFor_ServiceDeskProject() throws Exception {
		Project project = mockProject("SD");

		assertThat(stateService.areFeaturesEnabledFor(project), is(false));
	}

	@Test
	public void testAreFeaturesEnabledFor_BusinessProject() throws Exception {
		Project project = mockProject("BUS");

		assertThat(stateService.areFeaturesEnabledFor(project), is(false));
	}

	private MutableIssue mockIssue(String issueKey) {
		MutableIssue issue = mock(MutableIssue.class);
		when(issue.getKey()).thenReturn(issueKey);
		when(issue.getProjectObject()).then(invocation -> mockProject(extractProjectKeyFromIssueKey(issueKey)));
		return issue;
	}

	private Project mockProject(String projectKey) {
		Project project = mock(Project.class);
		when(project.getKey()).thenReturn(projectKey);
		return project;
	}
}
