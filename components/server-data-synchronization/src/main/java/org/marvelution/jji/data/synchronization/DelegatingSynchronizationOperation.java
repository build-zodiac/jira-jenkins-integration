/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.synchronization.api.SynchronizationOperationException;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.Syncable;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Named
public class DelegatingSynchronizationOperation {

	private final SiteSynchronizationOperation siteSynchronizationOperation;
	private final JobSynchronizationOperation jobSynchronizationOperation;

	@Inject
	public DelegatingSynchronizationOperation(SiteSynchronizationOperation siteSynchronizationOperation,
	                                          JobSynchronizationOperation jobSynchronizationOperation) {
		this.siteSynchronizationOperation = siteSynchronizationOperation;
		this.jobSynchronizationOperation = jobSynchronizationOperation;
	}

	void synchronize(Syncable syncable, DefaultProgress progress) throws SynchronizationOperationException {
		if (syncable instanceof Site) {
			synchronize((Site) syncable, progress);
		} else if (syncable instanceof Job) {
			synchronize((Job) syncable, progress);
		} else {
			throw new UnsupportedOperationException("Synchronization of '" + syncable + "' is not supported");
		}
	}

	private void synchronize(Site site, DefaultProgress progress) throws SynchronizationOperationException {
		siteSynchronizationOperation.synchronize(site, progress);
	}

	private void synchronize(Job job, DefaultProgress progress) throws SynchronizationOperationException {
		jobSynchronizationOperation.synchronize(job, progress);
	}

}
