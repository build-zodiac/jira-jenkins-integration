/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Progress;
import org.marvelution.jji.model.SyncProgress;

/**
 * Default Server implementation of the {@link Progress} API.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DefaultProgress implements Progress {

	private volatile Long timestampQueued;
	private volatile Long timestampStarted;
	private volatile Long timestampFinished;
	private volatile boolean shouldStop;
	private volatile int jobCount = 0;
	private volatile int buildCount = 0;
	private volatile int issueCount = 0;
	private volatile List<Throwable> errors = new ArrayList<>();
	private volatile List<String> errorMessages = new ArrayList<>();

	@Override
	public boolean isQueued() {
		return timestampQueued != null;
	}

	@Override
	public boolean hasStarted() {
		return timestampStarted != null;
	}

	@Override
	public boolean isFinished() {
		return timestampFinished != null;
	}

	@Override
	public boolean stopRequested() {
		return shouldStop;
	}

	@Override
	public SyncProgress asSyncProgress() {
		return new SyncProgress()
				.setFinished(isFinished())
				.setJobCount(jobCount)
				.setBuildCount(buildCount)
				.setIssueCount(issueCount)
				.setErrorCount(errors.size() + errorMessages.size());
	}

	@Override
	public void queued() {
		timestampQueued = System.currentTimeMillis();
	}

	@Override
	public void started() {
		timestampStarted = System.currentTimeMillis();
	}

	@Override
	public void finished() {
		timestampFinished = System.currentTimeMillis();
	}

	@Override
	public void shouldStop() {
		shouldStop = true;
	}

	@Override
	public void synchronizedJob(Job job) {
		jobCount++;
	}

	@Override
	public void synchronizedBuild(Build build) {
		buildCount++;
	}

	@Override
	public void linkedIssues(Set<IssueReference> references) {
		issueCount += references.size();
	}

	@Override
	public void addError(Throwable error) {
		errors.add(error);
	}

	@Override
	public void addError(String message) {
		errorMessages.add(message);
	}
}
