/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.synctoken.SyncTokenAuthenticator;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * REST Resource to get the base Url of the Jira Instance.
 *
 * @author Mark Rekveld
 * @since 3.2.1
 */
@AnonymousAllowed
@Path("base-url")
public class BaseUrlResource {

	private final SyncTokenAuthenticator syncTokenAuthenticator;
	private final ConfigurationService configurationService;

	public BaseUrlResource(SyncTokenAuthenticator syncTokenAuthenticator, ConfigurationService configurationService) {
		this.syncTokenAuthenticator = syncTokenAuthenticator;
		this.configurationService = configurationService;
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getBaseUrl(@Context HttpServletRequest request) {
		syncTokenAuthenticator.authenticate(request);
		return configurationService.getJIRABaseUrl().toASCIIString();
	}
}
