/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.admin;

import java.util.Comparator;

import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Site;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ConfigureJenkinsIntegration extends JiraWebActionSupport {

	private final StateService stateService;
	private final SiteService siteService;

	public ConfigureJenkinsIntegration(StateService stateService, SiteService siteService) {
		this.stateService = stateService;
		this.siteService = siteService;
	}

	@Override
	public String doDefault() throws Exception {
		return hasPermissions() ? INPUT : PERMISSION_VIOLATION_RESULT;
	}

	private boolean hasPermissions() {
		return hasGlobalPermission(GlobalPermissionKey.ADMINISTER);
	}

	/**
	 * Load all the {@link Site}s and sort them according to there names
	 */
	public Site[] loadSites() {
		return siteService.getAll().stream()
		                  .sorted(Comparator.comparing(Site::getName)).toArray(Site[]::new);
	}

	/**
	 * Returns the {@link StateService}
	 */
	public StateService getStateService() {
		return stateService;
	}

}
