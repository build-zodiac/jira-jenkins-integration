/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.rest.security.AdminRequired;

/**
 * REST resource for plugin configurations
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@AdminRequired
@Path("configuration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConfigurationResource {

	private final ConfigurationService configurationService;

	public ConfigurationResource(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	/**
	 * @since 2.0.0
	 */
	@GET
	public Configuration getConfiguration() {
		return configurationService.getConfiguration();
	}

	@POST
	public void saveConfiguration(Configuration configuration) {
		configurationService.saveConfiguration(configuration);
	}
}
