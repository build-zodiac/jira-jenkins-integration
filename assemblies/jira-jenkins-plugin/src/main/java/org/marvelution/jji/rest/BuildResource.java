/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.synctoken.SyncTokenAuthenticator;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * REST resource for the Jenkins plugin, with endpoints to trigger the synchronization of new builds and get issue link information.
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@AnonymousAllowed
@Path("build")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BuildResource {

	private final SyncTokenAuthenticator syncTokenAuthenticator;
	private final JobService jobService;
	private final BuildLinkService buildLinkService;

	public BuildResource(SyncTokenAuthenticator syncTokenAuthenticator, JobService jobService, BuildLinkService buildLinkService) {
		this.syncTokenAuthenticator = syncTokenAuthenticator;
		this.jobService = jobService;
		this.buildLinkService = buildLinkService;
	}

	/**
	 * Trigger the synchronization of a specific Job
	 *
	 * @param jobHash the hash of the Job URL name
	 */
	@PUT
	@Path("{jobHash}")
	public void synchronizeJob(@PathParam("jobHash") String jobHash, @Context HttpServletRequest request) {
		syncTokenAuthenticator.authenticate(request);
		jobService.synchronizeByHash(jobHash);
	}

	/**
	 * Returns a list of issues linked to the specific build
	 *
	 * @param jobHash     the hash of the Job URL name
	 * @param buildNumber the build number of the job
	 * @return 200 OK with a map of issue keys to issue urls
	 */
	@GET
	@Path("{jobHash}/{buildNumber}/links")
	public Map<String, String> getBuildLinks(@PathParam("jobHash") String jobHash, @PathParam("buildNumber") int buildNumber,
	                                         @Context HttpServletRequest request) {
		syncTokenAuthenticator.authenticate(request);
		return buildLinkService.getBuildLinks(jobHash, buildNumber);
	}
}
