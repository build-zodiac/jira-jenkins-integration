/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link PluginUpgradeTask} to regenerate shared secrets and trigger synchronization to push them to Jenkins.
 *
 * @author Mark Rekveld
 * @since 3.1.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T10_RegenerateSharedSecrets extends AbstractUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(T10_RegenerateSharedSecrets.class);
	private final SiteService siteService;

	@Inject
	public T10_RegenerateSharedSecrets(StateService stateService, SiteService siteService) {
		super(stateService);
		this.siteService = siteService;
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		siteService.getAll().stream().peek(site -> {
			LOGGER.info("Regenerating shared secret for site {} ({})", site.getName(), site.getId());
			site.setSharedSecret(SharedSecretGenerator.generate());
			site.setJenkinsPluginInstalled(false);
			site.setUseCrumbs(true);
		}).forEach(siteService::saveAndSynchronize);
		return null;
	}
}
