/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.jackson;

import java.io.IOException;

import org.marvelution.jji.model.Obfuscate;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.ser.std.SerializerBase;

/**
 * {@link String} {@link SerializerBase} to obfuscate the actual value with {@link Obfuscate#OBFUSCATED}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ObfuscationSerializer extends SerializerBase<String> {

	private final Obfuscate annotation;

	ObfuscationSerializer(Obfuscate annotation) {
		super(String.class);
		this.annotation = annotation;
	}

	@Override
	public void serialize(String value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		jgen.writeString(Obfuscate.OBFUSCATED);
	}

}
