/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.marvelution.jji.data.services.api.StateService;

import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

/**
 * Base class for {@link PluginUpgradeTask}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
abstract class AbstractUpgradeTask implements PluginUpgradeTask {

	private static final Pattern CLASS_NAME_PATTERN = Pattern.compile("^T([\\d]+)_(.*)$");
	protected final StateService stateService;
	private Integer buildNumber;
	private String description;

	AbstractUpgradeTask(StateService stateService) {
		this.stateService = stateService;
		Matcher matcher = CLASS_NAME_PATTERN.matcher(getClass().getSimpleName());
		if (matcher.matches()) {
			buildNumber = Integer.parseInt(matcher.group(1));
			description = stream(matcher.group(2).split("(?=\\p{Lu})")).collect(joining(" "));
		}
	}

	@Override
	public int getBuildNumber() {
		return Optional.ofNullable(buildNumber).orElseThrow(
				() -> new IllegalStateException("Missing buildNumber for " + getClass().getName()));
	}

	@Override
	public String getShortDescription() {
		return Optional.ofNullable(description).orElseGet(getClass()::getSimpleName);
	}

	@Override
	public String getPluginKey() {
		return stateService.getAddonKey();
	}
}
