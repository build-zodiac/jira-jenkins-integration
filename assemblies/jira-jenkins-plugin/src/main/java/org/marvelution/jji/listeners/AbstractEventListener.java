/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.listeners;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.atlassian.event.api.EventPublisher;

/**
 * Base Listener implementation that takes care or registration with the {@link EventPublisher}
 *
 * @author Mark Rekveld
 * @since 1.4.9
 */
public abstract class AbstractEventListener {

	private final EventPublisher eventPublisher;

	AbstractEventListener(EventPublisher eventPublisher) {
		this.eventPublisher = eventPublisher;
	}

	@PostConstruct
	public void registerWithPublisher() throws Exception {
		eventPublisher.register(this);
	}

	@PreDestroy
	public void unregisterWithPublisher() throws Exception {
		eventPublisher.unregister(this);
	}

}
