/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.model.Job;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * {@link PluginUpgradeTask} to cleanup default display names stored for builds.
 *
 * @author Mark Rekveld
 * @since 2.3.3
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T8_BuildDisplayNameCleanup extends AbstractUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(T8_BuildDisplayNameCleanup.class);
	private final JobService jobService;
	private final BuildService buildService;

	@Inject
	public T8_BuildDisplayNameCleanup(StateService stateService, JobService jobService, BuildService buildService) {
		super(stateService);
		this.jobService = jobService;
		this.buildService = buildService;
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		jobService.getAll(true).forEach(this::cleanupBuildsForJob);
		return null;
	}

	private void cleanupBuildsForJob(Job job) {
		buildService.getByJob(job).stream()
		            .filter(build -> {
			            String displayName = build.getDisplayName();
			            return displayName != null && displayName.endsWith(format("%s #%d", job.getName(), build.getNumber()));
		            })
		            .peek(build -> LOGGER.info("Cleaning up display name '{}' of {}", build.getDisplayName(), build))
		            .map(build -> {
			            build.setDisplayName(null);
			            return build;
		            })
		            .forEach(buildService::save);
	}
}
