/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.ManagedCache;
import org.mockito.Mock;

import static org.marvelution.jji.upgrade.T9_FlushOldCaches.UNUSED_CACHE_NAMES;

import static java.util.Arrays.stream;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link T9_FlushOldCaches}.
 *
 * @author Mark Rekveld
 * @since 2.3.5
 */
public class T9_FlushOldCachesTest extends AbstractUpgradeTaskTest<T9_FlushOldCaches> {

	@Mock
	private CacheManager cacheManager;

	@Override
	T9_FlushOldCaches createTask() {
		return new T9_FlushOldCaches(stateService,  cacheManager);
	}

	@Override
	public void testDoUpgrade() throws Exception {
		ManagedCache managedCache = mock(ManagedCache.class);
		stream(UNUSED_CACHE_NAMES).forEach(name -> when(cacheManager.getManagedCache(name)).thenReturn(managedCache));

		task.doUpgrade();

		verify(cacheManager, times(2)).getManagedCache(anyString());
		verify(managedCache, times(2)).clear();
	}
}
