/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.testing.TestSupport;

import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;

/**
 * Base tests for {@link PluginUpgradeTask}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public abstract class AbstractUpgradeTaskTest<T extends AbstractUpgradeTask> extends TestSupport {

	private static final String TEST_ADDON_KEY = "test.addon.key";
	@Mock
	StateService stateService;
	T task;

	@Before
	public void setUp() throws Exception {
		when(stateService.getAddonKey()).thenReturn(TEST_ADDON_KEY);
		task = createTask();
	}

	abstract T createTask();

	@Test
	public void testGetBuildNumber() throws Exception {
		assertThat(task.getBuildNumber(), greaterThanOrEqualTo(5));
	}

	@Test
	public void testGetShortDescription() throws Exception {
		assertThat(task.getShortDescription(), notNullValue());
	}

	@Test
	public void testGetPluginKey() throws Exception {
		assertThat(task.getPluginKey(), is(TEST_ADDON_KEY));
	}

	@Test
	public abstract void testDoUpgrade() throws Exception;
}
