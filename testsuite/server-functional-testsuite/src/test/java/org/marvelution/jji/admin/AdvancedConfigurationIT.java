/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import java.net.URI;

import org.marvelution.jji.elements.AdvancedConfigurationDialog;
import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.pages.JenkinsConfigurationPage;

import org.junit.Test;

import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Condition.visible;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Functional tests for the Advanced Configuration Dialog.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class AdvancedConfigurationIT extends AbstractAdminFunctionalTest {

	@Test
	public void testAdvancedConfiguration() throws Exception {
		loginAsAdminAndNavigateToJenkinsConfiguration();
		
		AdvancedConfigurationDialog dialog = JenkinsConfigurationPage.advancedConfiguration().open();

		dialog.header().shouldBe(visible);

		// Assert default configuration
		Configuration configuration = backdoor.configuration().getConfiguration();
		dialog.jiraBaseRpcUrl().shouldHave(value(configuration.getJIRABaseRpcUrl().toASCIIString()));
		dialog.maxBuildsPerPage().shouldHave(value(String.valueOf(configuration.getMaxBuildsPerPage())));
		dialog.useJobLatestBuildWhenIndexing().shouldNotBe(checked);

		// Assert validation error on invalid rpc url
		dialog.jiraBaseRpcUrl().val("localhost:2990/jira");
		dialog.submit()
		      .fieldError(dialog.jiraBaseRpcUrl()).shouldBe(visible).shouldHave(text("Invalid URL, missing host."));

		// Assert submission of configuration hides dialog
		dialog.jiraBaseRpcUrl().val("https://jira.example.com");
		dialog.maxBuildsPerPage().val("200");
		dialog.useJobLatestBuildWhenIndexing().click();
		dialog.submit();
		dialog.header().shouldNotBe(visible);

		// assert configuration is stored
		configuration = backdoor.configuration().getConfiguration();
		assertThat(configuration.getJIRABaseRpcUrl(), is(URI.create("https://jira.example.com")));
		assertThat(configuration.getMaxBuildsPerPage(), is(200));
		assertThat(configuration.isUseJobLatestBuildWhenIndexing(), is(true));

		// Assert new configuration is shown when opening dialog again
		dialog = JenkinsConfigurationPage.advancedConfiguration().open();

		dialog.jiraBaseRpcUrl().shouldHave(value("https://jira.example.com"));
		dialog.maxBuildsPerPage().shouldHave(value("200"));
		dialog.useJobLatestBuildWhenIndexing().shouldBe(checked);

		// Assert empty rpc url resets to base url
		dialog.jiraBaseRpcUrl().val("");
		dialog.submit();

		configuration = backdoor.configuration().getConfiguration();
		assertThat(configuration.getJIRABaseRpcUrl(), is(configuration.getJIRABaseUrl()));
		assertThat(configuration.getMaxBuildsPerPage(), is(200));
		assertThat(configuration.isUseJobLatestBuildWhenIndexing(), is(true));
	}
}
