/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.$;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class Flags {

	public static SelenideElement container() {
		return $("#aui-flag-container");
	}

	public static ElementsCollection flags() {
		return container().findAll(".aui-flag");
	}

	public static void hideAllFlags() {
		WebDriver driver = WebDriverRunner.getWebDriver();
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript("AJS.$('#aui-flag-container .aui-flag').attr('aria-hidden', 'true');");
		}
	}

}
