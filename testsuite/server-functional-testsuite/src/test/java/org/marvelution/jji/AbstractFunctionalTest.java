/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import org.marvelution.jji.elements.LoginPage;
import org.marvelution.jji.elements.UserOptions;
import org.marvelution.testing.FunctionalTestSupport;
import org.marvelution.jji.testkit.backdoor.Backdoor;

import com.codeborne.selenide.Configuration;
import org.junit.After;
import org.junit.BeforeClass;

import static com.codeborne.selenide.Condition.visible;

/**
 * Base test class for all Functional tests driven by Selenium and Selenide.
 *
 * @author Mark  Rekveld
 * @since 2.0.0
 */
public abstract class AbstractFunctionalTest extends FunctionalTestSupport {

	protected static Backdoor backdoor = new Backdoor();
	private static final String JIRA_BASE_URL = "jira.baseUrl";

	@BeforeClass
	public static void correctBaseUrl() {
		String jiraBaseUrl = backdoor.applicationProperties().getString(JIRA_BASE_URL);
		if (!jiraBaseUrl.equals(Configuration.baseUrl)) {
			backdoor.applicationProperties().setString(JIRA_BASE_URL, Configuration.baseUrl);
		}
	}

	@After
	public void reset() throws Exception {
		backdoor.sites().clearSites();
		backdoor.configuration().clearConfiguration();
	}

	protected void loginAsAdmin() {
		login("admin");
	}

	protected void login(String userpass) {
		login(userpass, userpass);
	}

	protected void login(String username, String password) {
		if (!LoginPage.form().is(visible)) {
			refreshOrOpen(LoginPage.URL);
		}
		LoginPage.form().shouldBe(visible);
		LoginPage.username().setValue(username);
		LoginPage.password().setValue(password);
		LoginPage.login().click();
		LoginPage.form().shouldNotBe(visible);
	}

	protected void logout() {
		try {
			new UserOptions().open().logout().shouldBe(visible).click();
		} catch (Exception e) {
			logger().error("Failed to logout; " + e.getMessage(), e);
		}
	}
}
