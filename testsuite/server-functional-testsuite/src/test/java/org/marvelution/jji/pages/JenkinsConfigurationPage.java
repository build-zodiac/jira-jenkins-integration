/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.pages;

import org.marvelution.jji.elements.AddSiteDialog;
import org.marvelution.jji.elements.AdvancedConfigurationDialog;
import org.marvelution.jji.elements.JenkinsSites;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JenkinsConfigurationPage {

	public static final String URL = "/secure/admin/ConfigureJenkinsIntegration!default.jspa";

	public static SelenideElement link() {
		return $("#jenkins_config");
	}

	public static SelenideElement actions() {
		return $("#plugin-actions");
	}

	public static AddSiteDialog addSite() {
		return new AddSiteDialog($("#add-site-button"));
	}

	public static AdvancedConfigurationDialog advancedConfiguration() {
		return new AdvancedConfigurationDialog($("#advanced-configuration-button"));
	}

	public static JenkinsSites sites() {
		return new JenkinsSites($("#jenkins-site-list").findAll(".jenkins-site-container"));
	}
}
