/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import org.marvelution.jji.pages.JenkinsConfigurationPage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * Element that describes a Jenkins job on the {@link JenkinsConfigurationPage}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JenkinsJob {

	private final SelenideElement container;

	public JenkinsJob(WebElement element) {
		this($(element));
	}

	public JenkinsJob(SelenideElement container) {
		this.container = container;
	}

	public JenkinsJob shouldHave(Condition... condition) {
		container.shouldHave(condition);
		return this;
	}
	
	public JenkinsJob shouldNotHave(Condition... condition) {
		container.shouldNotHave(condition);
		return this;
	}

	public String jobId() {
		return container.attr("jenkins-job-id");
	}

	public SelenideElement linked() {
		return getTD(0).find(By.tagName("input"));
	}

	public SelenideElement name() {
		return getTD(1);
	}

	public SelenideElement lastBuild() {
		return getTD(2);
	}

	public SelenideElement syncJob() {
		return getTD(3).find(".job-sync-icon");
	}

	public JenkinsJobActions actions() {
		return new JenkinsJobActions(getTD(4).find(By.tagName("a")));
	}

	private SelenideElement getTD(int index) {
		return container.find(By.tagName("td"), index);
	}
}
