/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.conditions;

import org.marvelution.jji.elements.JenkinsSite;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.WebElement;

/**
 * {@link Condition} to match if the {@link WebElement} is for a Site with a specific name.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class Site extends Condition {

	private final String name;

	Site(String name) {
		super("site");
		this.name = name;
	}

	@Override
	public boolean apply(WebElement element) {
		return new JenkinsSite(element).name().has(exactText(name));
	}

	@Override
	public String toString() {
		return super.name + " '" + name + '\'';
	}
}
