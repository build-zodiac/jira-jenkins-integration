/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.conditions;

import java.util.List;

import com.codeborne.selenide.ex.UIAssertionError;
import com.codeborne.selenide.impl.WebElementsCollection;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JobsMismatch extends UIAssertionError {

	private static final long serialVersionUID = 3436774332157612346L;

	JobsMismatch(WebElementsCollection collection, List<String> actualJobs, List<String> expectedJobs, long timeoutMs) {
		super("\nActual: " + actualJobs + "\nExpected: " + expectedJobs + "\nCollection: " + collection.description());
		super.timeoutMs = timeoutMs;
	}
}
