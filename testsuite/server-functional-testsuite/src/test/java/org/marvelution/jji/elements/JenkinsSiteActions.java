/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import com.codeborne.selenide.SelenideElement;

/**
 * Element that describes the Site actions drop down.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JenkinsSiteActions extends DropDown<JenkinsSiteActions> {

	JenkinsSiteActions(SelenideElement opener) {
		super(opener);
	}

	public EditSiteDialog editSite() {
		return new EditSiteDialog(dropDownItem(".edit-site"));
	}

	public DeleteSiteDialog deleteSite() {
		return new DeleteSiteDialog(dropDownItem(".delete-site"));
	}

	public ClearDeletedJobsDialog clearDeletedJobs() {
		return new ClearDeletedJobsDialog(dropDownItem(".delete-site-jobs"));
	}

	public SelenideElement refreshJobList() {
		return dropDownItem(".sync-site");
	}

	public SelenideElement autolinkSite() {
		return dropDownItem(".autolink-site");
	}
}
