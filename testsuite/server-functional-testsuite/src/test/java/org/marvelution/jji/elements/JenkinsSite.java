/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import org.marvelution.jji.pages.JenkinsConfigurationPage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;

/**
 * Element that describes a Jenkins Site on the {@link JenkinsConfigurationPage}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JenkinsSite {

	private final SelenideElement container;

	public JenkinsSite(WebElement element) {
		this($(element));
	}

	public JenkinsSite(SelenideElement container) {
		this.container = container;
	}

	public SelenideElement logo() {
		return container.find(".aui-page-header-image .aui-avatar-inner");
}

	public SelenideElement name() {
		return container.find(".aui-page-header-main");
	}

	public SelenideElement link() {
		return name().find(By.tagName("a"));
	}

	public JenkinsSiteActions actions() {
		return new JenkinsSiteActions(container.find(".aui-page-header-actions").find(By.tagName("button"), 0));
	}

	public JenkinsJobs jobs() {
		return new JenkinsJobs(container.find(".site-jobs-expanded").findAll(By.tagName("tr")).filter(attribute("jenkins-job-id")));
	}

	public MessageBar messageBar() {
		return new MessageBar(container.find(".message-bar"));
	}

	public JenkinsSite shouldBe(Condition... condition) {
		container.shouldBe(condition);
		return this;
	}

	public JenkinsSite shouldNotBe(Condition... condition) {
		container.shouldNotBe(condition);
		return this;
	}
}
