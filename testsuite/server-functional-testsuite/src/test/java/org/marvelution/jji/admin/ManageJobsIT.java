/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import java.util.Set;

import org.marvelution.jji.elements.DeleteJobDialog;
import org.marvelution.jji.elements.JenkinsJob;
import org.marvelution.jji.elements.JenkinsSite;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.pages.JenkinsConfigurationPage;
import org.marvelution.testing.wiremock.WireMockRule;

import org.junit.Rule;
import org.junit.Test;

import static org.marvelution.jji.model.Matchers.equalTo;

import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Functional tests for managing jobs on the Jenkins Configuration page.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ManageJobsIT extends AbstractAdminFunctionalTest {

	@Rule
	public WireMockRule jenkins = new WireMockRule().usingFilesUnderClass(ManageJobsIT.class);

	private Job createJob() {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());
		return backdoor.jobs().addJob(site, "jira-jenkins-integration");
	}

	@Test
	public void testLinkJob() throws Exception {
		Job job = createJob();

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		JenkinsJob jenkinsJob = jenkinsSite.jobs().jobById(job.getId());

		jenkinsJob.linked().shouldNotBe(checked).click();

		assertThat(backdoor.jobs().getJob(jenkinsJob.jobId()).isLinked(), is(true));

		jenkinsJob.linked().shouldBe(checked).click();

		assertThat(backdoor.jobs().getJob(jenkinsJob.jobId()).isLinked(), is(false));

		jenkinsJob.linked().shouldNotBe(checked).click();

		refreshOrOpenJenkinsConfiguration();

		jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		jenkinsJob = jenkinsSite.jobs().jobById(job.getId());

		jenkinsJob.linked().shouldBe(checked);

		assertThat(backdoor.jobs().getJob(jenkinsJob.jobId()).isLinked(), is(true));
	}

	@Test
	public void testSyncJob() throws Exception {
		Job job = createJob();

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		JenkinsJob jenkinsJob = jenkinsSite.jobs().jobById(job.getId());
		jenkinsJob.linked().shouldNotBe(checked);
		jenkinsJob.lastBuild().shouldHave(exactText("0"));

		jenkinsJob.syncJob().click();
		jenkinsJob.syncJob().shouldNotHave(cssClass("active"));
		jenkinsJob.lastBuild().shouldHave(exactText("0"));

		jenkinsJob.linked().click();
		jenkinsJob.syncJob().click();
		jenkinsJob.syncJob().shouldHave(cssClass("active"));

		waitForSynchronizationToComplete(() -> jenkinsJob.syncJob().shouldNotHave(cssClass("active")));

		jenkinsJob.syncJob().shouldNotHave(cssClass("active"));
		jenkinsJob.lastBuild().shouldHave(exactText("2"));

		Set<Build> builds = backdoor.builds().getBuilds(job);
		assertThat(builds, hasSize(1));
	}

	@Test
	public void testRestartSync() throws Exception {
		Job job = createJob();
		Build build1 = backdoor.builds().addBuild(job, "Backdoor build 1", Result.SUCCESS);
		Build build2 = backdoor.builds().addBuild(job, "Backdoor build 2", Result.SUCCESS);
		backdoor.jobs().saveJob(job.setLinked(true));

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		JenkinsJob jenkinsJob = jenkinsSite.jobs().jobById(job.getId());
		jenkinsJob.linked().shouldBe(checked);
		jenkinsJob.lastBuild().shouldHave(exactText(String.valueOf(build2.getNumber())));

		jenkinsJob.actions().open().restartJobSync().shouldBe(visible).click();
		jenkinsJob.lastBuild().shouldHave(exactText("0"));
		jenkinsJob.syncJob().shouldHave(cssClass("active"));

		waitForSynchronizationToComplete(() -> jenkinsJob.syncJob().shouldNotHave(cssClass("active")));

		jenkinsJob.linked().shouldBe(checked);
		jenkinsJob.lastBuild().shouldHave(exactText(String.valueOf(build2.getNumber())));

		Set<Build> builds = backdoor.builds().getBuilds(job);
		assertThat(builds, hasSize(2));
		assertThat(builds, hasItem(equalTo(build1.setDeleted(true))));
		assertThat(builds, not(hasItem(equalTo(build2))));
	}

	@Test
	public void testDeleteBuilds() throws Exception {
		Job job = createJob();
		backdoor.builds().addBuild(job, "Backdoor build 1", Result.SUCCESS);
		Build build2 = backdoor.builds().addBuild(job, "Backdoor build 2", Result.SUCCESS);
		backdoor.jobs().saveJob(job.setLinked(true));

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		JenkinsJob jenkinsJob = jenkinsSite.jobs().jobById(job.getId());
		jenkinsJob.linked().shouldBe(checked);
		jenkinsJob.lastBuild().shouldHave(exactText(String.valueOf(build2.getNumber())));

		jenkinsJob.actions().open().deleteJobBuilds().shouldBe(visible).click();

		jenkinsJob.linked().shouldBe(checked);
		jenkinsJob.lastBuild().shouldHave(exactText("0"));

		Set<Build> builds = backdoor.builds().getBuilds(job);
		assertThat(builds, empty());
	}

	@Test
	public void testDeleteJob() throws Exception {
		Job job = createJob();

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		JenkinsJob jenkinsJob = jenkinsSite.jobs().jobById(job.getId());

		jenkinsJob.actions().open().deleteJob().opener().shouldNotBe(visible);

		backdoor.jobs().saveJob(job.setDeleted(true));

		refreshOrOpenJenkinsConfiguration();

		jenkinsJob = jenkinsSite.jobs().jobById(job.getId());

		jenkinsJob.syncJob().shouldNotBe(visible);

		DeleteJobDialog dialog = jenkinsJob.actions().open().deleteJob().open();
		dialog.header().shouldHave(text("Clear Deleted Job:"), text(job.getDisplayName()));
		dialog.message().shouldHave(text("This action is not reversible."));
		dialog.close();

		jenkinsSite.jobs().shouldHaveJob(job.getDisplayName());

		jenkinsJob.actions().open().deleteJob().open().submit();

		jenkinsSite.jobs().shouldNotHaveJob(job.getDisplayName());

		refreshOrOpenJenkinsConfiguration();

		jenkinsSite.jobs().shouldNotHaveJob(job.getDisplayName());

		assertThat(backdoor.jobs().getJobs(), not(hasItem(equalTo(job))));
	}
}
