/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

/**
 * Base element that describes a dialog.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
@SuppressWarnings("unchecked")
abstract class Dialog<D extends Dialog<D>> {

	private final SelenideElement opener;
	private final String containerId;

	Dialog(SelenideElement opener, String id) {
		this.opener = opener;
		containerId = id;
	}

	public SelenideElement header() {
		return child(".aui-dialog2-header");
	}

	public SelenideElement content() {
		return child(".aui-dialog2-content");
	}

	public SelenideElement footer() {
		return child(".aui-dialog2-footer");
	}

	public SelenideElement opener() {
		return opener;
	}

	public D open() {
		Flags.hideAllFlags();
		opener.shouldBe(visible).click();
		header().shouldBe(visible);
		content().shouldBe(visible);
		footer().shouldBe(visible);
		return (D) this;
	}

	public D submit() {
		footer().find(".aui-dialog2-footer-actions .aui-button-primary").shouldBe(visible).click();
		return (D) this;
	}

	public D close() {
		footer().find(".aui-dialog2-footer-actions .aui-button-link").shouldBe(visible).click();
		header().shouldNotBe(visible);
		content().shouldNotBe(visible);
		footer().shouldNotBe(visible);
		return (D) this;
	}

	public SelenideElement fieldError(SelenideElement field) {
		return field.parent().find(".error");
	}

	protected SelenideElement dialog() {
		return $(containerId);
	}

	protected SelenideElement child(String selector) {
		return dialog().find(selector);
	}
}
