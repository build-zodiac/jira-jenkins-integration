/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@SuppressWarnings("unchecked")
abstract class DropDown<D extends DropDown<D>> {

	private static final String ARIA_EXPANDED = "aria-expanded";
	private final SelenideElement opener;
	private SelenideElement container;

	DropDown(String openerSelector) {
		this($(openerSelector));
	}

	DropDown(SelenideElement opener) {
		this.opener = opener;
	}

	public D open() {
		if (opener.attr(ARIA_EXPANDED).equals("false")) {
			Flags.hideAllFlags();
			opener.click();
			opener.shouldHave(attribute(ARIA_EXPANDED, "true"));
		}
		return (D) this;
	}

	public D close() {
		if (opener.attr(ARIA_EXPANDED).equals("true")) {
			opener.click();
			opener.shouldHave(attribute(ARIA_EXPANDED, "false"));
		}
		return (D) this;
	}

	SelenideElement dropDownItem(String selector) {
		return container().find(selector);
	}

	SelenideElement container() {
		if (container == null) {
			String id = opener.attr("aria-controls");
			if (id == null) {
				id = opener.attr("aria-owns");
			}
			container = $(By.id(id));
		}
		return container;
	}
}
