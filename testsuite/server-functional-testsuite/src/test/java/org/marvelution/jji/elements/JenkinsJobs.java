/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import org.marvelution.jji.conditions.Conditions;
import org.marvelution.jji.pages.JenkinsConfigurationPage;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Condition.attribute;

/**
 * Element that describes the Jenkins Job list of a {@link JenkinsSite} on the {@link JenkinsConfigurationPage}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JenkinsJobs {

	private final ElementsCollection jobContainers;

	public JenkinsJobs(ElementsCollection jobContainers) {
		this.jobContainers = jobContainers;
	}

	public JenkinsJobs shouldHaveSize(int expectedSize) {
		jobContainers.shouldHaveSize(expectedSize);
		return this;
	}

	public JenkinsJobs shouldBe(CollectionCondition... conditions) {
		jobContainers.shouldBe(conditions);
		return this;
	}

	public JenkinsJobs shouldHave(CollectionCondition... conditions) {
		jobContainers.shouldHave(conditions);
		return this;
	}

	public JenkinsJobs shouldHaveJob(String name) {
		jobContainers.filter(Conditions.job(name)).shouldHaveSize(1);
		return this;
	}

	public JenkinsJobs shouldNotHaveJob(String name) {
		jobContainers.filter(Conditions.job(name)).shouldHaveSize(0);
		return this;
	}

	public JenkinsJob job(String name) {
		return new JenkinsJob(jobContainers.find(Conditions.job(name)).shouldHave(attribute("jenkins-job-id")));
	}

	public JenkinsJob jobById(String jobId) {
		return new JenkinsJob(jobContainers.find(attribute("jenkins-job-id", jobId)));
	}
}
