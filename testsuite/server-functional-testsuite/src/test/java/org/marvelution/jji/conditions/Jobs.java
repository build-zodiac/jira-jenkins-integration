/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.conditions;

import java.util.List;

import org.marvelution.jji.elements.JenkinsJob;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ex.ElementNotFound;
import com.codeborne.selenide.impl.WebElementsCollection;
import org.openqa.selenium.WebElement;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

/**
 * {@link CollectionCondition} that matches {@link JenkinsJob} elements by {@link JenkinsJob#name()}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class Jobs extends CollectionCondition {

	private final List<String> expectedJobs;

	Jobs(String... expectedJobs) {
		this(asList(expectedJobs));
	}

	Jobs(List<String> expectedJobs) {
		if (expectedJobs.isEmpty()) {
			throw new IllegalArgumentException("No expected jobs given");
		}
		this.expectedJobs = expectedJobs;
	}

	@Override
	public boolean apply(List<WebElement> elements) {
		if (elements.size() != expectedJobs.size()) {
			return false;
		}
		for (WebElement element : elements) {
			JenkinsJob job = new JenkinsJob(element);
			if (!expectedJobs.contains(job.name().text())) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void fail(WebElementsCollection collection, List<WebElement> elements, Exception lastError, long timeoutMs) {
		if (elements == null || elements.isEmpty()) {
			ElementNotFound elementNotFound = new ElementNotFound(collection, expectedJobs, lastError);
			elementNotFound.timeoutMs = timeoutMs;
			throw elementNotFound;
		} else {
			List<String> actualJobs = elements.stream()
			                                   .map(JenkinsJob::new)
			                                   .map(job -> job.name().text())
			                                   .collect(toList());
			throw new JobsMismatch(collection, actualJobs, expectedJobs, timeoutMs);
		}
	}
}
