/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest.filter;

import java.util.List;
import javax.annotation.Nullable;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * @author Mark Rekveld
 * @since 2.3.0
 */
@Provider
public class BackdoorResourceFilterFactory implements ResourceFilterFactory {

	@Override
	public List<ResourceFilter> create(AbstractMethod am) {
		if (am.getResource().getResourceClass().getPackage().getName().startsWith("org.marvelution.jji.backdoor.rest")) {
			return singletonList(new BackdoorResourceFilter());
		} else {
			return emptyList();
		}
	}

	@Provider
	public static class BackdoorResourceFilter implements ResourceFilter, ContainerRequestFilter {

		private final static Logger LOGGER = LoggerFactory.getLogger(BackdoorResourceFilter.class);

		@Override
		public ContainerRequest filter(ContainerRequest request) {
			LOGGER.warn("Intercepted REST request to Jenkins Backdoor API: {}", request.getRequestUri());
			return request;
		}

		@Override
		public ContainerRequestFilter getRequestFilter() {
			return this;
		}

		@Override
		@Nullable
		public ContainerResponseFilter getResponseFilter() {
			return null;
		}

	}

}
