/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.Set;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@AnonymousAllowed
@Path("build")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorBuildResource {

	private final BuildService buildService;
	private final BuildLinkService buildLinkService;

	public BackdoorBuildResource(@ComponentImport BuildService buildService, @ComponentImport BuildLinkService buildLinkService) {
		this.buildService = buildService;
		this.buildLinkService = buildLinkService;
	}

	@POST
	public Build save(Build job) {
		return buildService.save(job);
	}

	@Nullable
	@GET
	@Path("{buildId}")
	public Build get(@PathParam("buildId") String buildId) {
		return buildService.get(buildId).orElse(null);
	}

	@GET
	@Path("all/{jobId}")
	public Set<Build> getAll(@PathParam("jobId") String jobId) {
		return buildService.getAllInRange(new Job().setId(jobId), 0, -1);
	}

	@POST
	@Path("{buildId}/link")
	public void linkBuildToIssue(@PathParam("buildId") String buildId, String issueKey) {
		buildService.get(buildId).ifPresent(build -> buildLinkService.link(build, issueKey));
	}

	@Nullable
	@GET
	@Path("{buildId}/linked")
	public Set<String> getRelatedIssueKeys(@PathParam("buildId") String buildId) {
		Build build = get(buildId);
		if (build != null) {
			return buildLinkService.getRelatedIssueKeys(build);
		} else {
			return null;
		}
	}

	@DELETE
	@Path("{buildId}")
	public void delete(@PathParam("buildId") String buildId) {
		buildService.get(buildId).ifPresent(buildService::delete);
	}

}
