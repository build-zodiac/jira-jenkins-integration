/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Component for disabling addons that can interfere with testing.
 *
 * @author Mark Rekveld
 * @since 3.3.0
 */
@Named
@ExportAsService(value = LifecycleAware.class)
public class AddonDisabler implements LifecycleAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(AddonDisabler.class);
	private static final String[] ADDON_KEYS = { "com.atlassian.plugins.helptips.jira-help-tips",
	                                             "com.atlassian.jira.jira-onboarding-assets-plugin",
	                                             "com.atlassian.feedback.jira-feedback-plugin" };
	private final PluginAccessor pluginAccessor;

	@Inject
	public AddonDisabler(@ComponentImport PluginAccessor pluginAccessor) {
		this.pluginAccessor = pluginAccessor;
	}

	@Override
	public void onStart() {
		for (String addonKey : ADDON_KEYS) {
			Plugin plugin = pluginAccessor.getPlugin(addonKey);
			if (plugin != null) {
				try {
					plugin.disable();
					LOGGER.info("Disabled addon {}", addonKey);
				} catch (PluginException e) {
					LOGGER.error("Failed to disable addon {}: {}", addonKey, e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public void onStop() {
	}
}
