/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.junit.Before;

import static org.marvelution.jji.rest.AbstractResourceAuthzTest.Method.DELETE;
import static org.marvelution.jji.rest.AbstractResourceAuthzTest.Method.GET;
import static org.marvelution.jji.rest.AbstractResourceAuthzTest.Method.POST;
import static org.marvelution.jji.rest.AbstractResourceAuthzTest.Method.PUT;
import static org.marvelution.jji.rest.Matchers.status;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Base Integration Testcase for Authentication and Authorization REST resources testing
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public abstract class AbstractResourceAuthzTest extends AbstractResourceTest {

	@Before
	public void setUpAnonymousDefaultAuth() {
		anonymous();
	}

	ClientResponse testAuthzGet(WebResource resource, AuthzPair authz) {
		return testAuthz(GET, resource, authz);
	}

	ClientResponse testAuthzGet(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair authz) {
		return testAuthz(GET, resource, resourceCustomizer, authz);
	}

	ClientResponse testAuthzPost(WebResource resource, AuthzPair authz) {
		return testAuthz(POST, resource, authz);
	}

	ClientResponse testAuthzPost(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair authz) {
		return testAuthz(POST, resource, resourceCustomizer, authz);
	}

	ClientResponse testAuthzPut(WebResource resource, AuthzPair authz) {
		return testAuthz(PUT, resource, authz);
	}

	ClientResponse testAuthzPut(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair authz) {
		return testAuthz(PUT, resource, resourceCustomizer, authz);
	}

	ClientResponse testAuthzDelete(WebResource resource, AuthzPair authz) {
		return testAuthz(DELETE, resource, authz);
	}

	ClientResponse testAuthzDelete(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair authz) {
		return testAuthz(DELETE, resource, resourceCustomizer, authz);
	}

	private ClientResponse testAuthz(Method method, WebResource resource, AuthzPair authz) {
		return testAuthz(method, resource, AuthzWebResourceBuilderCustomizer.DEFAULT, authz);
	}

	private ClientResponse testAuthz(Method method, WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer,
	                                 AuthzPair authz) {
		if (authz.filter != null) {
			// Add the required filter for this authz test
			resource.addFilter(authz.filter);
		}
		ClientResponse response = resourceCustomizer.customize(resource.getRequestBuilder()).method(method.name(), ClientResponse.class);
		assertThat("Response status mismatch", response, status(authz.expectedResponseStatus));
		assertThat(resource, notNullValue());
		return response;
	}

	/**
	 * Builtin {@link AuthzPair} for anonymous access
	 */
	AuthzPair anonymous(ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(ANONYMOUS, expectedResponseStatus);
	}

	/**
	 * Builtin {@link AuthzPair} for authenticated access
	 */
	AuthzPair authenticatedUser(ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(USER, expectedResponseStatus);
	}

	/**
	 * Builtin {@link AuthzPair} for administrator access
	 */
	AuthzPair administrator(ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(ADMIN, expectedResponseStatus);
	}

	/**
	 * Builtin {@link AuthzPair} for controlling access using the specified {@link ClientFilter}.
	 */
	AuthzPair filtered(ClientFilter filter, ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(filter, expectedResponseStatus);
	}

	/**
	 * Customizer API for authz resource invocations
	 */
	interface AuthzWebResourceBuilderCustomizer {

		AuthzWebResourceBuilderCustomizer DEFAULT = original -> original;

		WebResource.Builder customize(WebResource.Builder original);
	}

	enum Method {
		GET, PUT, POST, DELETE
	}

	/**
	 * Authentication test pair
	 */
	class AuthzPair {

		final ClientFilter filter;
		final ClientResponse.Status expectedResponseStatus;

		private AuthzPair(String userpass, ClientResponse.Status expectedResponseStatus) {
			this(new HTTPBasicAuthFilter(userpass, userpass), expectedResponseStatus);
		}

		private AuthzPair(ClientFilter filter, ClientResponse.Status expectedResponseStatus) {
			this.filter = filter;
			this.expectedResponseStatus = expectedResponseStatus;
		}
	}
}
