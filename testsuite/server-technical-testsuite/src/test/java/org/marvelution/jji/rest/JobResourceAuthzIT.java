/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;

import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Integration Authz Testcase for the job REST resource.
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class JobResourceAuthzIT extends AbstractResourceAuthzTest {

	private Job job;

	@Before
	public void setUp() throws Exception {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
		job = backdoor.jobs().addJob(site, testName.getMethodName());
	}

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	@Test
	public void testGetAll_Anonymous() throws Exception {
		testAuthzGet(jobResource(), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetAll_AuthenticatedUser() throws Exception {
		testAuthzGet(jobResource(), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testGetAll_Administrator() throws Exception {
		testAuthzGet(jobResource(), administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testGetJob_Anonymous() throws Exception {
		testAuthzGet(jobResource(job), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetJob_AuthenticatedUser() throws Exception {
		testAuthzGet(jobResource(job), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testGetJob_Administrator() throws Exception {
		testAuthzGet(jobResource(job), administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testRemoveJob_Anonymous() throws Exception {
		job.setDeleted(true);
		backdoor.jobs().saveJob(job);
		testAuthzDelete(jobResource(job), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testRemoveJob_AuthenticatedUser() throws Exception {
		job.setDeleted(true);
		backdoor.jobs().saveJob(job);
		testAuthzDelete(jobResource(job), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testRemoveJob_Administrator() throws Exception {
		job.setDeleted(true);
		backdoor.jobs().saveJob(job);
		testAuthzDelete(jobResource(job), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testSyncJob_Anonymous() throws Exception {
		testAuthzPut(jobResource(job, "sync"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testSyncJob_AuthenticatedUser() throws Exception {
		testAuthzPut(jobResource(job, "sync"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testSyncJob_Administrator() throws Exception {
		testAuthzPut(jobResource(job, "sync"), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testEnableJobLink_Anonymous() throws Exception {
		testAuthzPost(jobResource(job, "link", "true"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testEnableJobLink_AuthenticatedUser() throws Exception {
		testAuthzPost(jobResource(job, "link", "true"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testEnableJobLink_Administrator() throws Exception {
		testAuthzPost(jobResource(job, "link", "true"), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testRemoveAllBuilds_Anonymous() throws Exception {
		testAuthzDelete(jobResource(job, "builds"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testRemoveAllBuilds_AuthenticatedUser() throws Exception {
		testAuthzDelete(jobResource(job, "builds"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testRemoveAllBuilds_Administrator() throws Exception {
		testAuthzDelete(jobResource(job, "builds"), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testRebuildJobCache_Anonymous() throws Exception {
		testAuthzPost(jobResource(job, "rebuild"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testRebuildJobCache_AuthenticatedUser() throws Exception {
		testAuthzPost(jobResource(job, "rebuild"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testRebuildJobCache_Administrator() throws Exception {
		testAuthzPost(jobResource(job, "rebuild"), administrator(ClientResponse.Status.NO_CONTENT));
	}
}
