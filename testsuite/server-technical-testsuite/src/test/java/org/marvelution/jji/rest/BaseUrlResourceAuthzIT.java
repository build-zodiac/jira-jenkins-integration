/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;

import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.synctoken.jersey.SyncTokenClientFilter;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static java.util.Optional.of;

/**
 * Integration Authz Testcase for the base-url REST resource.
 *
 * @author Mark Rekveld
 * @since 3.2.1
 */
public class BaseUrlResourceAuthzIT extends AbstractResourceAuthzTest {

	private Site site;

	@Before
	public void setUp() {
		site = backdoor.sites().addSite(SiteType.JENKINS, "Local Test", URI.create("http://localhost:8080"));
		backdoor.sites().populateSharedSecret(site);
	}

	@After
	public void tearDown() {
		backdoor.sites().clearSites();
	}

	@Test
	public void testGetBaseUrl_Anonymous() {
		testAuthzGet(baseUrlResource(), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetBaseUrl_AuthenticatedUser() {
		testAuthzGet(baseUrlResource(), authenticatedUser(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetBaseUrl_Administrator() {
		testAuthzGet(baseUrlResource(), administrator(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetBaseUrl() {
		testAuthzGet(baseUrlResource(),
		             filtered(new SyncTokenClientFilter(site.getId(), site.getSharedSecret(), of(backdoor.environmentData().getContext())),
		                      ClientResponse.Status.OK));
	}
}
