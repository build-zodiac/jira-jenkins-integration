/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration Authz Testcase for the site REST resource
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class SiteResourceAuthzIT extends AbstractResourceAuthzTest {

	private Site site;

	@Before
	public void setUp() throws Exception {
		site = backdoor.sites().addSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
	}

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	@Test
	public void testGetAll_Anonymous() throws Exception {
		testAuthzGet(siteResource(), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetAll_AuthenticatedUser() throws Exception {
		testAuthzGet(siteResource(), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testGetAll_Administrator() throws Exception {
		testAuthzGet(siteResource(), administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testGetSite_Anonymous() throws Exception {
		testAuthzGet(siteResource(site), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testGetSite_AuthenticatedUser() throws Exception {
		testAuthzGet(siteResource(site), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testGetSite_Administrator() throws Exception {
		testAuthzGet(siteResource(site), administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testCRUD() {
		ClientResponse response = testAuthzPost(siteResource(),
		                                        original -> {
			                                        Site site = new Site();
			                                        site.setType(SiteType.JENKINS);
			                                        site.setName("authz-add");
			                                        site.setRpcUrl(URI.create("http://localhost:8081"));
			                                        return original.entity(site, MediaType.APPLICATION_JSON_TYPE);
		                                        },
		                                        administrator(ClientResponse.Status.OK));
		final Site site = response.getEntity(Site.class);
		assertThat(site.getName(), is("authz-add"));
		response = testAuthzPost(siteResource(site),
		                         original -> {
			                         site.setName("authz-update");
			                         return original.entity(site, MediaType.APPLICATION_JSON_TYPE);
		                         },
		                         administrator(ClientResponse.Status.OK));
		final Site updated = response.getEntity(Site.class);
		assertThat(updated.getName(), is("authz-update"));
		testAuthzDelete(siteResource(updated), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testRead_Anonymous() throws Exception {
		testAuthzPost(siteResource(),
		              original -> original.entity(new Site().setName("Name"), MediaType.APPLICATION_JSON_TYPE),
		              anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testRead_AuthenticatedUser() throws Exception {
		testAuthzPost(siteResource(),
		              original -> original.entity(new Site().setName("Name"), MediaType.APPLICATION_JSON_TYPE),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testUpdate_Anonymous() throws Exception {
		testAuthzPost(siteResource(site),
		              original -> original.entity(site.setName("Updated name"), MediaType.APPLICATION_JSON_TYPE),
		              anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testUpdate_AuthenticatedUser() throws Exception {
		testAuthzPost(siteResource(site),
		              original -> original.entity(site.setName("Updated name"), MediaType.APPLICATION_JSON_TYPE),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testDelete_Anonymous() throws Exception {
		testAuthzDelete(siteResource(site), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testDelete_AuthenticatedUser() throws Exception {
		testAuthzDelete(siteResource(site), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testSyncJobList_Anonymous() throws Exception {
		testAuthzPost(siteResource(site, "sync"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testSyncJobList_AuthenticatedUser() throws Exception {
		testAuthzPost(siteResource(site, "sync"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testSyncJobList_Administrator() throws Exception {
		testAuthzPost(siteResource(site, "sync"), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testEnableAutoLink_Anonymous() throws Exception {
		testAuthzPost(siteResource(site, "autolink/true"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testEnableAutoLink_AuthenticatedUser() throws Exception {
		testAuthzPost(siteResource(site, "autolink/true"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testEnableAutoLink_Administrator() throws Exception {
		testAuthzPost(siteResource(site, "autolink/true"), administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testSiteStatus_Anonymous() throws Exception {
		testAuthzGet(siteResource(site, "status"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testSiteStatus_AuthenticatedUser() throws Exception {
		testAuthzGet(siteResource(site, "status"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testSiteStatus_Administrator() throws Exception {
		testAuthzGet(siteResource(site, "status"), administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testRemoveJobs_Anonymous() throws Exception {
		testAuthzDelete(siteResource(site, "jobs"), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testRemoveJobs_AuthenticatedUser() throws Exception {
		testAuthzDelete(siteResource(site, "jobs"), authenticatedUser(ClientResponse.Status.FORBIDDEN));
	}

	@Test
	public void testRemoveJobs_Administrator() throws Exception {
		testAuthzDelete(siteResource(site, "jobs"), administrator(ClientResponse.Status.NO_CONTENT));
	}
}
