/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;

import org.marvelution.jji.data.services.api.ConfigurationService;

import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import org.hamcrest.Matcher;
import org.junit.Before;

import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link DefaultSiteClient}.
 * 
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DefaultSiteClientIT extends AbstractBaseSiteClientIT<DefaultSiteClient> {

	@Inject
	private ConfigurationService configurationService;

	@Override
	public void configure(Binder binder) {
		binder.bind(ConfigurationService.class).toInstance(mock(ConfigurationService.class));
		binder.bind(HttpClientRequestFactory.class).in(Scopes.SINGLETON);
		binder.bind(new TypeLiteral<NonMarshallingRequestFactory<?>>() {}).to(HttpClientRequestFactory.class);

		binder.bind(BaseSiteClient.class).to(DefaultSiteClient.class);
	}

	@Before
	public void setUpMocks() throws Exception {
		when(configurationService.getJIRABaseRpcUrl()).thenReturn(URI.create("http://jira.example.com"));
		when(configurationService.getJIRAInstanceName()).thenReturn("JIRA Server");
	}

	@Override
	protected Matcher<Object> baseRpcUrlMatcher() {
		return is(fromUri(configurationService.getJIRABaseRpcUrl()).path("rest").path("jenkins").path("latest").build().toASCIIString());
	}
}
