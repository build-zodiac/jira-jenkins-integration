/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.marvelution.jji.data.access.ActiveObjectTransactionTestRuleFactory;
import org.marvelution.jji.data.access.DataAccessModule;
import org.marvelution.jji.data.access.api.BuildDAO;
import org.marvelution.jji.data.access.api.IssueDAO;
import org.marvelution.jji.data.access.api.JobDAO;
import org.marvelution.jji.data.access.api.SiteDAO;
import org.marvelution.jji.data.access.model.v1.BuildMapping;
import org.marvelution.jji.data.access.model.v1.IssueMapping;
import org.marvelution.jji.data.access.model.v1.JobMapping;
import org.marvelution.jji.data.access.model.v1.SiteMapping;
import org.marvelution.jji.data.access.model.v1.TestResultsMapping;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.model.TestResults;
import org.marvelution.testing.inject.TestInjector;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ModelVersion;
import com.google.inject.Binder;
import com.google.inject.Module;
import net.java.ao.DBParam;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.Jdbc;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.marvelution.jji.model.Matchers.equalTo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

/**
 * Testcase for {@link To_1_ModelV2Migrator}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Jdbc
@Data(To_1_ModelV2MigratorIT.SetupModelV0.class)
public class To_1_ModelV2MigratorIT implements Module {

	private EntityManager entityManager;
	@Rule
	public TestRule transactionRule = ActiveObjectTransactionTestRuleFactory.create(this);
	@Rule
	public TestRule testInjector = new TestInjector(this);
	@Inject
	private ActiveObjects activeObjects;
	@Inject
	private To_1_ModelV2Migrator underTest;
	@Inject
	private SiteDAO siteDAO;
	@Inject
	private JobDAO jobDAO;
	@Inject
	private BuildDAO buildDAO;
	@Inject
	private IssueDAO issueDAO;

	@Test
	public void testUpgrade() throws Exception {
		underTest.upgrade(ModelVersion.valueOf("0"), activeObjects);

		List<Site> sites = siteDAO.getAll();
		assertThat(sites, hasSize(3));
		assertSiteNamedJenkins(assertAndGetSite(sites, new Site().setName("Jenkins")
		                                                         .setType(SiteType.JENKINS)
		                                                         .setRpcUrl(URI.create("http://localhost:8080/"))
		                                                         .setAutoLinkNewJobs(true)
		                                                         .setJenkinsPluginInstalled(true)));

		assertSiteNamedHudson(assertAndGetSite(sites, new Site().setName("Hudson")
		                                                        .setType(SiteType.HUDSON)
		                                                        .setRpcUrl(URI.create("http://localhost:8081/"))
		                                                        .setAutoLinkNewJobs(true)
		                                                        .setUseCrumbs(true)));

		assertUnnamedSite(assertAndGetSite(sites, new Site().setName("JENKINS at http://localhost:8082/")
		                                                    .setType(SiteType.JENKINS)
		                                                    .setRpcUrl(URI.create("http://localhost:8082/"))
		                                                    .setDisplayUrl(URI.create("http://localhost/"))
		                                                    .setUser("admin")
		                                                    .setToken("admin123")
		                                                    .setJenkinsPluginInstalled(true)));
	}

	private void assertSiteNamedJenkins(Site site) {
		List<Job> jobs = jobDAO.getAllBySiteId(site.getId(), true);
		assertThat(jobs, hasSize(2));

		Job jenkinsCore = assertAndGetItem(jobs, equalTo(new Job().setSite(site)
		                                                          .setName("jenkins-core")
		                                                          .setLastBuild(1)
		                                                          .setLinked(true), true));
		assertThat(buildDAO.getAllByJob(jenkinsCore.getId()), hasSize(1));
		Build jenkinsCoreBuild = buildDAO.get(jenkinsCore.getId(), 1);
		assertThat(jenkinsCoreBuild, equalTo(new Build().setJob(jenkinsCore)
		                                                .setNumber(1)
		                                                .setCause("SCM Change")
		                                                .setResult(Result.SUCCESS)
		                                                .setDuration(135329L)
		                                                .setTimestamp(1355405446078L)
		                                                .setTestResults(new TestResults().setTotal(10)), true));

		Job libraries = assertAndGetItem(jobs, equalTo(new Job().setSite(site)
		                                                        .setName("libraries")
		                                                        .setUrlName("core/job/libraries")
		                                                        .setDisplayName("Libraries")
		                                                        .setLastBuild(1)
		                                                        .setLinked(true), true));
		assertThat(buildDAO.getAllByJob(libraries.getId()), hasSize(1));
		Build librariesBuild = buildDAO.get(libraries.getId(), 1);
		assertThat(librariesBuild, equalTo(new Build().setJob(libraries)
		                                              .setNumber(1)
		                                              .setCause("Triggered by Admin")
		                                              .setDisplayName("Manual Build")
		                                              .setResult(Result.FAILURE)
		                                              .setBuiltOn("Slave #1")
		                                              .setDuration(235329L)
		                                              .setTimestamp(1351545179000L)
		                                              .setTestResults(new TestResults().setFailed(10)
		                                                                               .setSkipped(5)
		                                                                               .setTotal(100)), true));

		Set<Build> byIssueKey = buildDAO.getByIssueKey("MIGR-1");
		assertThat(byIssueKey, hasSize(2));
		assertThat(byIssueKey, hasItems(jenkinsCoreBuild, librariesBuild));
		byIssueKey = buildDAO.getByIssueKey("MIGR-2");
		assertThat(byIssueKey, hasSize(1));
		assertThat(byIssueKey, hasItem(jenkinsCoreBuild));
		byIssueKey = buildDAO.getByIssueKey("RGIM-1");
		assertThat(byIssueKey, hasSize(1));
		assertThat(byIssueKey, hasItem(librariesBuild));
		Set<Build> byProjectKey = buildDAO.getByProjectKey("MIGR");
		assertThat(byProjectKey, hasSize(2));
		assertThat(byProjectKey, hasItems(jenkinsCoreBuild, librariesBuild));
		byProjectKey = buildDAO.getByProjectKey("RGIM");
		assertThat(byProjectKey, hasSize(1));
		assertThat(byProjectKey, hasItems(librariesBuild));

		Set<String> issueKeysByBuild = issueDAO.getIssueKeysByBuild(jenkinsCoreBuild.getId());
		assertThat(issueKeysByBuild, hasSize(2));
		assertThat(issueKeysByBuild, hasItems("MIGR-1", "MIGR-2"));
		issueKeysByBuild = issueDAO.getIssueKeysByBuild(librariesBuild.getId());
		assertThat(issueKeysByBuild, hasSize(2));
		assertThat(issueKeysByBuild, hasItems("MIGR-1", "RGIM-1"));

		Set<String> projectKeysByBuild = issueDAO.getProjectKeysByBuild(jenkinsCoreBuild.getId());
		assertThat(projectKeysByBuild, hasSize(1));
		assertThat(projectKeysByBuild, hasItem("MIGR"));
		projectKeysByBuild = issueDAO.getProjectKeysByBuild(librariesBuild.getId());
		assertThat(projectKeysByBuild, hasSize(2));
		assertThat(projectKeysByBuild, hasItems("MIGR", "RGIM"));
	}

	private void assertSiteNamedHudson(Site site) {
		List<Job> jobs = jobDAO.getAllBySiteId(site.getId(), true);
		assertThat(jobs, hasSize(1));

		Job hudsonCore = assertAndGetItem(jobs, equalTo(new Job().setSite(site)
		                                                         .setName("hudson-core")
		                                                         .setUrlName("hudson/job/hudson-core")
		                                                         .setDisplayName("hudson - hudson-core")
		                                                         .setDeleted(true), true));

		assertThat(buildDAO.getAllByJob(hudsonCore.getId()), hasSize(0));
	}

	private void assertUnnamedSite(Site site) {
		assertThat(jobDAO.getAllBySiteId(site.getId(), true), hasSize(0));
	}

	private Site assertAndGetSite(Collection<Site> sites, Site expectedSite) {
		Site site = assertAndGetItem(sites, equalTo(expectedSite, true));
		assertThat(site.getSharedSecret(), is(notNullValue()));
		return site;
	}

	private <T> T assertAndGetItem(Collection<T> items, Matcher<T> matcher) {
		assertThat(items, hasItem(matcher));
		return items.stream().filter(matcher::matches).findFirst().orElseThrow(AssertionError::new);
	}

	@Override
	public void configure(Binder binder) {
		binder.install(new DataAccessModule(() -> entityManager));
		binder.bind(To_1_ModelV2Migrator.class).in(Singleton.class);
	}

	public static class SetupModelV0 implements DatabaseUpdater {

		@Override
		@SuppressWarnings({ "unchecked", "deprecation" })
		public void update(EntityManager entityManager) throws Exception {
			entityManager.migrate(
					BuildMapping.class,
					IssueMapping.class,
					JobMapping.class,
					SiteMapping.class,
					TestResultsMapping.class
			);

			entityManager.create(SiteMapping.class,
			                     new DBParam("ID", 1),
			                     new DBParam(SiteMapping.NAME, "Jenkins"),
			                     new DBParam(SiteMapping.SITE_TYPE, "JENKINS"),
			                     new DBParam(SiteMapping.RPC_URL, "http://localhost:8080/"),
			                     new DBParam(SiteMapping.AUTO_LINK, true),
			                     new DBParam(SiteMapping.SUPPORTS_BACK_LINK, true));
			entityManager.create(SiteMapping.class,
			                     new DBParam("ID", 2),
			                     new DBParam(SiteMapping.NAME, "Hudson"),
			                     new DBParam(SiteMapping.SITE_TYPE, "HUDSON"),
			                     new DBParam(SiteMapping.RPC_URL, "http://localhost:8081/"),
			                     new DBParam(SiteMapping.AUTO_LINK, true),
			                     new DBParam(SiteMapping.USE_CRUMBS, true));
			entityManager.create(SiteMapping.class,
			                     new DBParam("ID", 3),
			                     new DBParam(SiteMapping.SITE_TYPE, "JENKINS"),
			                     new DBParam(SiteMapping.RPC_URL, "http://localhost:8082/"),
			                     new DBParam(SiteMapping.DISPLAY_URL, "http://localhost/"),
			                     new DBParam(SiteMapping.USER, "admin"),
			                     new DBParam(SiteMapping.USER_TOKEN, "admin123"),
			                     new DBParam(SiteMapping.SUPPORTS_BACK_LINK, true));

			entityManager.create(JobMapping.class,
			                     new DBParam("ID", 1),
			                     new DBParam(JobMapping.SITE_ID, 1),
			                     new DBParam(JobMapping.NAME, "jenkins-core"),
			                     new DBParam(JobMapping.LAST_BUILD, 1),
			                     new DBParam(JobMapping.LINKED, true));
			entityManager.create(JobMapping.class,
			                     new DBParam("ID", 2),
			                     new DBParam(JobMapping.SITE_ID, 2),
			                     new DBParam(JobMapping.NAME, "hudson-core"),
			                     new DBParam(JobMapping.URL_NAME, "hudson/job/hudson-core"),
			                     new DBParam(JobMapping.DELETED, true));
			entityManager.create(JobMapping.class,
			                     new DBParam("ID", 3),
			                     new DBParam(JobMapping.SITE_ID, 1),
			                     new DBParam(JobMapping.NAME, "libraries"),
			                     new DBParam(JobMapping.URL_NAME, "core/job/libraries"),
			                     new DBParam(JobMapping.DISPLAY_NAME, "Libraries"),
			                     new DBParam(JobMapping.LAST_BUILD, 1),
			                     new DBParam(JobMapping.LINKED, true));

			entityManager.create(BuildMapping.class,
			                     new DBParam("ID", 1),
			                     new DBParam(BuildMapping.JOB_ID, 1),
			                     new DBParam(BuildMapping.BUILD_NUMBER, 1),
			                     new DBParam(BuildMapping.CAUSE, "SCM Change"),
			                     new DBParam(BuildMapping.RESULT, "SUCCESS"),
			                     new DBParam(BuildMapping.DURATION, 135329L),
			                     new DBParam(BuildMapping.TIME_STAMP, 1355405446078L));
			entityManager.create(BuildMapping.class,
			                     new DBParam("ID", 2),
			                     new DBParam(BuildMapping.JOB_ID, 3),
			                     new DBParam(BuildMapping.BUILD_NUMBER, 1),
			                     new DBParam(BuildMapping.CAUSE, "Triggered by Admin"),
			                     new DBParam(BuildMapping.DISPLAY_NAME, "Manual Build"),
			                     new DBParam(BuildMapping.RESULT, "FAILURE"),
			                     new DBParam(BuildMapping.DURATION, 235329L),
			                     new DBParam(BuildMapping.TIME_STAMP, 1351545179000L),
			                     new DBParam(BuildMapping.BUILT_ON, "Slave #1"));

			entityManager.create(TestResultsMapping.class,
			                     new DBParam("ID", 1),
			                     new DBParam(TestResultsMapping.BUILD_ID, 1),
			                     new DBParam(TestResultsMapping.FAILED, 0),
			                     new DBParam(TestResultsMapping.SKIPPED, 0),
			                     new DBParam(TestResultsMapping.TOTAL, 10));
			entityManager.create(TestResultsMapping.class,
			                     new DBParam("ID", 2),
			                     new DBParam(TestResultsMapping.BUILD_ID, 2),
			                     new DBParam(TestResultsMapping.FAILED, 10),
			                     new DBParam(TestResultsMapping.SKIPPED, 5),
			                     new DBParam(TestResultsMapping.TOTAL, 100));

			entityManager.create(IssueMapping.class,
			                     new DBParam("ID", 1),
			                     new DBParam(IssueMapping.JOB_ID, 1),
			                     new DBParam(IssueMapping.BUILD_ID, 1),
			                     new DBParam(IssueMapping.BUILD_DATE, 1355405446078L),
			                     new DBParam(IssueMapping.ISSUE_KEY, "MIGR-1"),
			                     new DBParam(IssueMapping.PROJECT_KEY, "MIGR"));
			entityManager.create(IssueMapping.class,
			                     new DBParam("ID", 2),
			                     new DBParam(IssueMapping.JOB_ID, 1),
			                     new DBParam(IssueMapping.BUILD_ID, 1),
			                     new DBParam(IssueMapping.BUILD_DATE, 1355405446078L),
			                     new DBParam(IssueMapping.ISSUE_KEY, "MIGR-2"),
			                     new DBParam(IssueMapping.PROJECT_KEY, "MIGR"));
			entityManager.create(IssueMapping.class,
			                     new DBParam("ID", 3),
			                     new DBParam(IssueMapping.JOB_ID, 2),
			                     new DBParam(IssueMapping.BUILD_ID, 2),
			                     new DBParam(IssueMapping.BUILD_DATE, 1355405446078L),
			                     new DBParam(IssueMapping.ISSUE_KEY, "RGIM-1"),
			                     new DBParam(IssueMapping.PROJECT_KEY, "RGIM"));
			entityManager.create(IssueMapping.class,
			                     new DBParam("ID", 4),
			                     new DBParam(IssueMapping.JOB_ID, 2),
			                     new DBParam(IssueMapping.BUILD_ID, 2),
			                     new DBParam(IssueMapping.BUILD_DATE, 1351545179000L),
			                     new DBParam(IssueMapping.ISSUE_KEY, "MIGR-1"),
			                     new DBParam(IssueMapping.PROJECT_KEY, "MIGR"));
		}
	}
}
