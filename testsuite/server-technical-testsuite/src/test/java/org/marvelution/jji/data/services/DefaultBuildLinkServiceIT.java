/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import org.marvelution.jji.data.access.ActiveObjectTransactionTestRuleFactory;
import org.marvelution.jji.data.access.DataAccessModule;
import org.marvelution.jji.data.access.ModelDatabaseUpdater;
import org.marvelution.jji.data.services.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.events.BuildIssueLinksUpdatedEvent;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ProjectDeletedEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.google.inject.Binder;
import com.google.inject.Inject;
import com.google.inject.util.Modules;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.Jdbc;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.ArgumentCaptor;

import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link DefaultBuildLinkService}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Jdbc
@Data(ModelDatabaseUpdater.class)
public class DefaultBuildLinkServiceIT extends AbstractBaseBuildLinkServiceIT<DefaultBuildLinkService> {

	@Rule
	public TestRule transactionRule = ActiveObjectTransactionTestRuleFactory.create(this);
	private EntityManager entityManager;
	@Inject
	private IssueReferenceProvider issueReferenceProvider;
	@Inject
	private EventPublisher eventPublisher;
	private Set<IssueReference> issueReferences;

	@Override
	public void configure(Binder binder) {
		binder.install(new DataAccessModule(() -> entityManager));
		binder.install(Modules.override(new DataServicesModule()).with(new AbstractModule() {
			@Override
			protected void configure() {
				bindMock(IssueReferenceProvider.class);
			}
		}));
	}

	@Before
	@SuppressWarnings("unchecked")
	public void setUpMocks() throws Exception {
		issueReferences = new HashSet<>();
		when(issueReferenceProvider.getIssueReference(anyString())).then(invocation -> {
			String issueKey = invocation.getArgumentAt(0, String.class);
			return issueReferences.stream().filter(reference -> reference.getIssueKey().equals(issueKey)).findFirst();
		});
		when(issueReferenceProvider.getIssueReferences(any(Set.class))).then(invocation -> {
			Set<String> issueKeys = invocation.getArgumentAt(0, Set.class);
			return issueReferences.stream().filter(reference -> issueKeys.contains(reference.getIssueKey())).collect(toSet());
		});
		doAnswer(invocation -> {
			IssueReference reference = invocation.getArgumentAt(0, IssueReference.class);
			reference.setIssueUrl(URI.create("https://jira.example.com/browse/" + reference.getIssueKey()));
			return null;
		}).when(issueReferenceProvider).setIssueUrl(any(IssueReference.class));
	}

	@Override
	protected void setupIssueReference(String... issueKeys) {
		requireNonNull(issueKeys);
		stream(issueKeys).map(key -> new IssueReference().setIssueKey(key).setProjectKey(extractProjectKeyFromIssueKey(key)))
		                 .forEach(issueReferences::add);
	}

	@Override
	public void testLink() throws Exception {
		super.testLink();
		ArgumentCaptor<BuildIssueLinksUpdatedEvent> captor = ArgumentCaptor.forClass(BuildIssueLinksUpdatedEvent.class);
		verify(eventPublisher, times(3)).publish(captor.capture());
		assertThat(captor.getAllValues(), hasSize(3));
		assertThat(captor.getAllValues().get(0).getIssueKeys(), hasSize(1));
		assertThat(captor.getAllValues().get(0).getIssueKeys(), hasItem("PR-1"));
		assertThat(captor.getAllValues().get(1).getIssueKeys(), hasSize(1));
		assertThat(captor.getAllValues().get(1).getIssueKeys(), hasItem("PR-1"));
		assertThat(captor.getAllValues().get(2).getIssueKeys(), hasSize(1));
		assertThat(captor.getAllValues().get(2).getIssueKeys(), hasItem("PR-2"));
	}

	@Override
	public void testRelink() throws Exception {
		super.testRelink();
		ArgumentCaptor<BuildIssueLinksUpdatedEvent> captor = ArgumentCaptor.forClass(BuildIssueLinksUpdatedEvent.class);
		verify(eventPublisher, times(3)).publish(captor.capture());
		assertThat(captor.getAllValues(), hasSize(3));
		assertThat(captor.getAllValues().get(0).getIssueKeys(), hasSize(2));
		assertThat(captor.getAllValues().get(0).getIssueKeys(), hasItems("PR-1", "PR-2"));
		assertThat(captor.getAllValues().get(1).getIssueKeys(), hasSize(1));
		assertThat(captor.getAllValues().get(1).getIssueKeys(), hasItem("OP-1"));
		assertThat(captor.getAllValues().get(2).getIssueKeys(), hasSize(0));
	}

	@Test
	public void testOnIssueEvent() throws Exception {
		Build build = setupBuild(createBuild(job),
		                         new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("PR-3").setProjectKey("PR"));

		assertThat(buildLinkService.getIssueLinkCount(build), is(3));
		Set<String> issueKeysByBuild = buildLinkService.getRelatedIssueKeys(build);
		assertThat(issueKeysByBuild, hasSize(3));
		assertThat(issueKeysByBuild, hasItems("PR-1", "PR-2", "PR-3"));

		Issue issue = mock(Issue.class);
		when(issue.getKey()).thenReturn("PR-1");
		buildLinkService.onIssueEvent(new IssueEvent(issue, null, null, EventType.ISSUE_UPDATED_ID));

		assertThat(buildLinkService.getIssueLinkCount(build), is(3));
		issueKeysByBuild = buildLinkService.getRelatedIssueKeys(build);
		assertThat(issueKeysByBuild, hasSize(3));
		assertThat(issueKeysByBuild, hasItems("PR-1", "PR-2", "PR-3"));

		buildLinkService.onIssueEvent(new IssueEvent(issue, null, null, EventType.ISSUE_DELETED_ID));

		assertThat(buildLinkService.getIssueLinkCount(build), is(2));
		issueKeysByBuild = buildLinkService.getRelatedIssueKeys(build);
		assertThat(issueKeysByBuild, hasSize(2));
		assertThat(issueKeysByBuild, hasItems("PR-2", "PR-3"));
	}

	@Test
	public void testOnProjectDeleted() throws Exception {
		Build build = setupBuild(createBuild(job),
		                         new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                         new IssueReference().setIssueKey("OP-1").setProjectKey("OP"));

		Set<String> projectKeys = buildLinkService.getRelatedProjectKeys(build);
		assertThat(projectKeys, hasSize(2));
		assertThat(projectKeys, hasItems("PR", "OP"));

		Project project = mock(Project.class);
		when(project.getKey()).thenReturn("PR");
		buildLinkService.onProjectDeleted(new ProjectDeletedEvent(null, project));

		projectKeys = buildLinkService.getRelatedProjectKeys(build);
		assertThat(projectKeys, hasSize(1));
		assertThat(projectKeys, hasItems("OP"));
	}
}
