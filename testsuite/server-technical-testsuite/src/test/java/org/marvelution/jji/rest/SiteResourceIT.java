/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.ErrorMessages;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.testing.wiremock.WireMockRule;

import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import com.sun.jersey.api.client.ClientResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.model.Matchers.errorForField;
import static org.marvelution.jji.model.Obfuscate.OBFUSCATED;
import static org.marvelution.jji.rest.Matchers.badRequest;
import static org.marvelution.jji.rest.Matchers.status;
import static org.marvelution.jji.rest.Matchers.successful;

import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class SiteResourceIT extends AbstractResourceTest {

	private static final TypeReference<Map<String, Object>> MAP_TYPE_REFERENCE = new TypeReference<Map<String, Object>>() {};
	@Rule
	public WireMockRule jenkins = new WireMockRule();

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	@Test
	public void testAddSite() throws Exception {
		Site site = new Site();

		// try adding a site without details
		ClientResponse response = siteResource().entity(site, APPLICATION_JSON_TYPE).post(ClientResponse.class);
		assertThat(response, badRequest());
		ErrorMessages errors = response.getEntity(ErrorMessages.class);
		assertThat(errors.getErrors(), hasSize(3));
		assertThat(errors.getErrors(), hasItem(errorForField("name")));
		assertThat(errors.getErrors(), hasItem(errorForField("type")));
		assertThat(errors.getErrors(), hasItem(errorForField("rpcUrl")));

		// try adding a site with invalid rpcUrl
		site.setType(SiteType.JENKINS);
		site.setName("Added CI");
		site.setRpcUrl(URI.create("localhost"));
		site.setUser("bob");
		site.setToken("bob");
		response = siteResource().entity(site, APPLICATION_JSON_TYPE).post(ClientResponse.class);
		assertThat(response, status(ClientResponse.Status.BAD_REQUEST));
		errors = response.getEntity(ErrorMessages.class);
		assertThat(errors.getErrors(), hasSize(1));
		assertThat(errors.getErrors(), hasItem(errorForField("rpcUrl")));

		// try adding a site with valid rpcUrl
		site.setRpcUrl(jenkins.serverUri());
		response = siteResource().entity(site, APPLICATION_JSON_TYPE).post(ClientResponse.class);
		assertThat(response, successful());

		// verify returned site object
		final Site entity = response.getEntity(Site.class);
		assertThat(entity, is(notNullValue()));
		assertThat(entity.getId(), is(notNullValue()));
		assertThat(entity.getName(), is("Added CI"));
		assertThat(entity.getSharedSecret(), is(OBFUSCATED));
		assertThat(entity.getType(), is(SiteType.JENKINS));
		assertThat(entity.getRpcUrl(), is(jenkins.serverUri()));
		assertThat(entity.getUser(), is("bob"));
		assertThat(entity.getToken(), is(OBFUSCATED));
		assertThat(entity.isJenkinsPluginInstalled(), is(false));
		assertThat(entity.isUseCrumbs(), is(true));

		// verify that jobs are synchronized after the entity is added
		assertThat(entity, is(equalTo(backdoor.sites().getSite(entity.getId()))));
		await().atMost(20, TimeUnit.SECONDS).until(() -> !backdoor.sites().getSite(entity.getId()).getJobs().isEmpty());
		List<Job> jobs = backdoor.sites().getSite(entity.getId()).getJobs();
		assertThat(jobs, hasSize(3));
		assertThat(jobs.stream().filter(Job::isLinked).count(), is(0L));
		jenkins.verifyGET(urlPathEqualTo("/plugin/jenkins-jira-plugin/ping.html"));
		jenkins.verifyGET(urlPathEqualTo("/api/json/"));
	}

	@Test
	public void testUpdateSite() throws Exception {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "Updated CI", URI.create("http://localhost:8080/"));
		site.setRpcUrl(jenkins.serverUri());
		site.setUser("bob");
		site.setToken("bob");
		site.setAutoLinkNewJobs(true);
		Site entity = siteResource(site).entity(site, APPLICATION_JSON_TYPE).post(Site.class);
		assertThat(entity.isJenkinsPluginInstalled(), is(false));
		assertThat(entity.isUseCrumbs(), is(true));
		assertThat(entity, is(equalTo(backdoor.sites().getSite(site.getId()))));
		await().atMost(20, TimeUnit.SECONDS).until(() -> !backdoor.sites().getSite(entity.getId()).getJobs().isEmpty());
		Site updated = backdoor.sites().getSite(entity.getId());
		assertThat(updated.isJenkinsPluginInstalled(), is(false));
		assertThat(updated.isUseCrumbs(), is(false));
		assertThat(updated.getJobs(), hasSize(3));
		assertThat(updated.getJobs().stream().filter(Job::isLinked).count(), is(3L));
		jenkins.verifyGET(urlPathEqualTo("/plugin/jenkins-jira-plugin/ping.html"));
		jenkins.verifyGET(urlPathEqualTo("/api/json/"));
		jenkins.verifyGET(urlPathMatching("/job/([a-z-\\-]+)/api/json/"));
	}

	@Test
	public void testRegisterAndUnregisterWithSite() throws Exception {
		// Add new Site.
		Site site = siteResource().entity(new Site().setType(SiteType.JENKINS)
		                                            .setName("Register - Unregister")
		                                            .setRpcUrl(jenkins.serverUri()), APPLICATION_JSON_TYPE).post(Site.class);
		assertThat(site.getSharedSecret(), is(OBFUSCATED));

		// Get Site SharedSecret - is obfuscated in REST API.
		backdoor.sites().populateSharedSecret(site);
		Configuration configuration = backdoor.configuration().getConfiguration();

		await().atMost(15, TimeUnit.SECONDS)
		       .until(() -> siteResource(site, "sync", "status").get(Site.class).getProgress().isFinished());

		jenkins.verifyGET(urlPathEqualTo("/plugin/jenkins-jira-plugin/ping.html"));
		jenkins.verifyGET(urlPathEqualTo("/api/json/"));

		// Verify registration request.
		List<LoggedRequest> requests = jenkins.client().find(postRequestedFor(urlPathEqualTo("/plugin/jenkins-jira-plugin/register/")));
		assertThat(requests, hasSize(1));
		LoggedRequest registerRequest = requests.get(0);
		Map<String, Object> registerBody = new ObjectMapper().readValue(registerRequest.getBody(), MAP_TYPE_REFERENCE);
		assertThat(registerBody.get("url"), is(fromUri(configuration.getJIRABaseRpcUrl()).path("rest").path("jenkins").path("latest")
		                                                                                 .build().toASCIIString()));
		assertThat(registerBody.get("name"), is(configuration.getJIRAInstanceName()));
		assertThat(registerBody.get("identifier"), is(site.getId()));
		assertThat(registerBody.get("sharedSecret"), is(site.getSharedSecret()));

		// Delete the site to trigger unregistration.
		siteResource(site).delete();

		// Verify unregistration request.
		requests = jenkins.client().find(postRequestedFor(urlPathEqualTo("/plugin/jenkins-jira-plugin/unregister/")));
		assertThat(requests, hasSize(1));
		LoggedRequest unregisterRequest = requests.get(0);
		Map<String, Object> unregisterBody = new ObjectMapper().readValue(unregisterRequest.getBody(), MAP_TYPE_REFERENCE);
		assertThat(unregisterBody.get("url"), is(fromUri(configuration.getJIRABaseRpcUrl()).path("rest").path("jenkins").path("latest")
		                                                                                   .build().toASCIIString()));
	}
}
