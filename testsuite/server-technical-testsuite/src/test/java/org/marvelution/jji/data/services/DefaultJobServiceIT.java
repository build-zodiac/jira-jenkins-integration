/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import org.marvelution.jji.data.access.ActiveObjectTransactionTestRuleFactory;
import org.marvelution.jji.data.access.DataAccessModule;
import org.marvelution.jji.data.access.ModelDatabaseUpdater;

import com.google.inject.Binder;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.Jdbc;
import org.junit.Rule;
import org.junit.rules.TestRule;

/**
 * Tests for {@link DefaultJobService}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Jdbc
@Data(ModelDatabaseUpdater.class)
public class DefaultJobServiceIT extends AbstractBaseJobServiceIT<DefaultJobService> {

	private EntityManager entityManager;
	@Rule
	public TestRule transactionRule = ActiveObjectTransactionTestRuleFactory.create(this);

	@Override
	public void configure(Binder binder) {
		binder.install(new DataAccessModule(() -> entityManager));
		binder.install(new DataServicesModule());
	}
}
