/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.services.api.SiteValidator;
import org.marvelution.jji.data.services.api.TextResolver;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.inject.Module;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;

/**
 * Guice {@link Module} for data services tests.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DataServicesModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(SiteService.class).to(DefaultSiteService.class).in(Scopes.SINGLETON);
		bind(JobService.class).to(DefaultJobService.class).in(Scopes.SINGLETON);
		bind(BuildService.class).to(DefaultBuildService.class).in(Scopes.SINGLETON);
		bind(BuildLinkService.class).to(DefaultBuildLinkService.class).in(Scopes.SINGLETON);
		bind(ConfigurationService.class).to(DefaultConfigurationService.class).in(Scopes.SINGLETON);
		bind(IssueReferenceProvider.class).to(DefaultIssueReferenceProvider.class).in(Scopes.SINGLETON);
		bind(SiteClient.class).to(DefaultSiteClient.class).in(Scopes.SINGLETON);
		bind(TextResolver.class).to(DefaultTextResolver.class).in(Scopes.SINGLETON);
		bind(SiteValidator.class).to(DefaultSiteValidator.class).in(Scopes.SINGLETON);

		bind(NonMarshallingMockRequestFactory.class).in(Scopes.SINGLETON);
		bind(new TypeLiteral<NonMarshallingRequestFactory<?>>() {}).to(NonMarshallingMockRequestFactory.class);

		bindMock(SynchronizationService.class);

		bindMock(ApplicationProperties.class);
		bindMock(EventPublisher.class);
		bindMock(I18nResolver.class);
		bindMock(IssueManager.class);
		bindMock(PluginSettingsFactory.class);
	}
}
