/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.lang.reflect.Method;

import net.java.ao.schema.FieldNameConverter;
import net.java.ao.schema.IndexNameConverter;
import net.java.ao.schema.SequenceNameConverter;
import net.java.ao.schema.TableNameConverter;
import net.java.ao.schema.TriggerNameConverter;
import net.java.ao.schema.UniqueNameConverter;
import net.java.ao.test.jdbc.JdbcConfiguration;
import net.java.ao.test.junit.ActiveObjectTransactionMethodRule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

/**
 * {@link TestRule} version of {@link ActiveObjectTransactionMethodRule}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ActiveObjectTransactionTestRule extends ActiveObjectTransactionMethodRule implements TestRule {

	ActiveObjectTransactionTestRule(Object test, JdbcConfiguration jdbc, boolean withIndex, TableNameConverter tableNameConverter,
	                                FieldNameConverter fieldNameConverter, SequenceNameConverter sequenceNameConverter,
	                                TriggerNameConverter triggerNameConverter, IndexNameConverter indexNameConverter,
	                                UniqueNameConverter uniqueNameConverter) {
		super(test, jdbc, withIndex, tableNameConverter, fieldNameConverter, sequenceNameConverter, triggerNameConverter,
		      indexNameConverter, uniqueNameConverter);
	}

	@Override
	public Statement apply(Statement base, Description description) {
		try {
			Method method = description.getTestClass().getMethod(description.getMethodName());
			return apply(base, new FrameworkMethod(method), null);
		} catch (NoSuchMethodException e) {
			throw new AssertionError("Failed to create FrameworkMethod for test " + description, e);
		}
	}
}
