/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jji.AbstractIntegrationTest;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.DeserializationConfig;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

/**
 * Base Integration Testcase for REST resource testing
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public abstract class AbstractResourceTest extends AbstractIntegrationTest {

	@SuppressWarnings("Duplicates")
	private static ThreadLocal<Client> client = ThreadLocal.withInitial(() -> {
		DefaultClientConfig config = new DefaultClientConfig();
		JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
		jacksonProvider.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		config.getSingletons().add(jacksonProvider);
		return Client.create(config);
	});
	private ClientFilter authFilter = new HTTPBasicAuthFilter(ADMIN, ADMIN);

	protected void anonymous() {
		authFilter = null;
	}

	protected void asAdmin() {
		loginAs(ADMIN);
	}

	protected void loginAs(String username) {
		loginAs(username, username);
	}

	protected void loginAs(String username, String password) {
		loginWith(new HTTPBasicAuthFilter(username, password));
	}

	protected void loginWith(ClientFilter filter) {
		authFilter = filter;
	}

	/**
	 * Returns the {@link WebResource site resource}
	 */
	protected WebResource siteResource(String... path) {
		return siteResource(null, path);
	}

	/**
	 * Returns the {@link WebResource site resource}
	 */
	protected WebResource siteResource(@Nullable Site site, String... path) {
		Map<String, Object> values = new HashMap<>();
		UriBuilder uriBuilder = uriBuilder().path("site");
		if (site != null) {
			uriBuilder.path("{siteId}");
			values.put("siteId", site.getId());
		}
		if (path != null && path.length > 0) {
			for (String p : path) {
				uriBuilder.path(p);
			}
		}
		return resource(uriBuilder.buildFromMap(values));
	}

	/**
	 * Returns the {@link WebResource job resource}
	 */
	protected WebResource jobResource(String... path) {
		return jobResource(null, path);
	}

	/**
	 * Returns the {@link WebResource job resource}
	 */
	protected WebResource jobResource(@Nullable Job job, String... path) {
		Map<String, Object> values = new HashMap<>();
		UriBuilder uriBuilder = uriBuilder().path("job");
		if (job != null) {
			uriBuilder.path("{jobId}");
			values.put("jobId", job.getId());
		}
		if (path != null && path.length > 0) {
			for (String p : path) {
				uriBuilder.path(p);
			}
		}
		return resource(uriBuilder.buildFromMap(values));
	}

	/**
	 * Returns the {@link WebResource build resource}
	 */
	protected WebResource buildResource(Job job, String... path) {
		return buildResource(job, -1, path);
	}

	/**
	 * Returns the {@link WebResource build resource}
	 */
	protected WebResource buildResource(Job job, int buildNumber, String... path) {
		Map<String, Object> values = new HashMap<>();
		UriBuilder uriBuilder = uriBuilder().path("build").path("{jobHash}");
		values.put("jobHash", sha1Hex(job.getUrlName()));
		if (buildNumber > -1) {
			uriBuilder.path("{buildNumber}");
			values.put("buildNumber", buildNumber);
		}
		if (path != null && path.length > 0) {
			for (String p : path) {
				uriBuilder.path(p);
			}
		}
		return resource(uriBuilder.buildFromMap(values));
	}

	/**
	 * Returns the {@link WebResource build resource}
	 */
	protected WebResource buildResource(Build build, String... path) {
		return buildResource(build.getJob(), build.getNumber(), path);
	}

	/**
	 * Returns the {@link WebResource configuration resource}
	 */
	protected WebResource configurationResource() {
		return resource(uriBuilder().path("configuration").build());
	}

	/**
	 * Returns the {@link WebResource release-report resource}
	 */
	protected WebResource releaseReportResource() {
		return resource(uriBuilder().path("release-report").build());
	}

	/**
	 * Returns the {@link WebResource base-url resource}
	 */
	protected WebResource baseUrlResource() {
		return resource(uriBuilder().path("base-url").build());
	}

	/**
	 * Returns a new {@link UriBuilder} to build {@link URI}s targeted to the Jenkins API
	 */
	protected UriBuilder uriBuilder() {
		try {
			return UriBuilder.fromUri(backdoor.environmentData().getBaseUrl().toURI()).path("rest").path("jenkins").path("1.0");
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Malformed base url " + e.getMessage(), e);
		}
	}

	private WebResource resource(URI uri) {
		WebResource resource = client().resource(uri);
		if (authFilter != null) {
			resource.addFilter(authFilter);
		}
		return resource;
	}

	private Client client() {
		return client.get();
	}

}
