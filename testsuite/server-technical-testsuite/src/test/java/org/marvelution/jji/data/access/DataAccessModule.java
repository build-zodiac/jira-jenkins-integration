/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import javax.inject.Provider;

import org.marvelution.jji.data.access.api.BuildDAO;
import org.marvelution.jji.data.access.api.IssueDAO;
import org.marvelution.jji.data.access.api.JobDAO;
import org.marvelution.jji.data.access.api.SiteDAO;
import org.marvelution.testing.inject.AbstractModule;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.google.inject.Module;
import com.google.inject.Scopes;
import net.java.ao.EntityManager;

import static java.lang.reflect.Proxy.newProxyInstance;
import static java.util.Optional.ofNullable;

/**
 * Guice {@link Module} for data access tests.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DataAccessModule extends AbstractModule implements InvocationHandler {

	private final Provider<EntityManager> entityManager;
	private ActiveObjects activeObjects;

	public DataAccessModule(Provider<EntityManager> entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	protected void configure() {
		bind(ActiveObjects.class).toInstance(
				(ActiveObjects) newProxyInstance(getClass().getClassLoader(), new Class[] { ActiveObjects.class }, this));
		bind(SiteDAO.class).to(DefaultSiteDAO.class).in(Scopes.SINGLETON);
		bind(JobDAO.class).to(DefaultJobDAO.class).in(Scopes.SINGLETON);
		bind(BuildDAO.class).to(DefaultBuildDAO.class).in(Scopes.SINGLETON);
		bind(IssueDAO.class).to(DefaultIssueDAO.class).in(Scopes.SINGLETON);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (activeObjects == null) {
			activeObjects = ofNullable(entityManager.get()).map(TestActiveObjects::new)
			                                               .orElseThrow(() -> new IllegalStateException("No EntityManager available"));
		}
		return method.invoke(activeObjects, args);
	}
}
