/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import org.marvelution.jji.model.Configuration;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.3.0
 */
public class ConfigurationControl extends BackdoorControl<ConfigurationControl> {

	ConfigurationControl(JIRAEnvironmentData environmentData) {
		super(environmentData);
	}

	public Configuration getConfiguration() {
		return createConfigurationResource().get(Configuration.class);
	}

	public Configuration saveConfiguration(Configuration configuration) {
		return createConfigurationResource().type(APPLICATION_JSON_TYPE)
		                                    .accept(APPLICATION_JSON_TYPE)
		                                    .post(Configuration.class, configuration);
	}

	/**
	 * @since 3.0.0
	 */
	public void clearConfiguration() {
		createConfigurationResource().delete();
	}

	public Configuration useJobLatestBuildWhenIndexing(boolean useJobLatestBuildWhenIndexing) {
		Configuration configuration = getConfiguration();
		configuration.setUseJobLatestBuildWhenIndexing(useJobLatestBuildWhenIndexing);
		return saveConfiguration(configuration);
	}
}
